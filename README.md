## Lola (discontinued)

Lola is a Raspberry Pi powered toy tank that can be driven First Person View from an Android application (or from it's self-hosted [web app](https://bitbucket.org/vgabor99/webpi))

![](https://bitbucket.org/vgabor99/webpi/raw/c82493da1d6d73eec4bbea0aa40a0c4120e93ea8/app/static/img/img_6456.jpg)

## Motivation

Having fun and amusing my house guests. As a side effect, practicing Python and learn WebRTC.

## Technical details

Lola uses [GStreamer](https://gstreamer.freedesktop.org/) and [raspivid](https://www.raspberrypi.org/app/uploads/2013/07/RaspiCam-Documentation.pdf) to receive / and send the video stream. The available streaming configurations are received from the stream source (both connection-oriented and connectionless modes are supported). The control channel is JSON-RPC over TCP.

## Discontinuation

The project has served its purpose, I am no longer interested maintaining it.
