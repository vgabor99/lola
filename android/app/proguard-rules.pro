# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable,Signature,*Annotation*

-dontpreverify
-repackageclasses 'com.vargag99.lola'
-allowaccessmodification
-optimizations !code/simplification/arithmetic

# Methods and inst vars referenced from native.

-keep class com.vargag99.lola.gstreamer.Player {
    long mNativeCustomData;
    void onError(java.lang.String);
    void onStateChanged(int);
    void onGStreamerInitialized();
}

##-------------Begin Settings For Guava ------------------------------------------------------------
#https://github.com/krschultz/android-proguard-snippets/blob/master/libraries/proguard-guava.pro
# Configuration for Guava
#
# disagrees with instructions provided by Guava project: https://code.google.com/p/guava-libraries/wiki/UsingProGuardWithGuava
#
# works if you add the following line to the Gradle dependencies
#
# provided 'javax.annotation:jsr250-api:1.0'
-keep class com.google.common.io.Resources {
    public static <methods>;
}
-keep class com.google.common.collect.Lists {
    public static ** reverse(**);
}
-keep class com.google.common.base.Charsets {
    public static <fields>;
}

-keep class com.google.common.base.Joiner {
    public static Joiner on(String);
    public ** join(...);
}

-keep class com.google.common.collect.MapMakerInternalMap$ReferenceEntry
-keep class com.google.common.cache.LocalCache$ReferenceEntry

-dontwarn sun.misc.Unsafe
-dontwarn javax.annotation.**

##-------------End Settings For Guava --------------------------------------------------------------
