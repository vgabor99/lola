package com.vargag99.lola.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.vargag99.lola.R;
import com.vargag99.lola.generic.Util;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Joystick view.
 */
public class JoystickView extends View {
    private Paint mPaint = new Paint();
    private boolean mGrabbed;
    private PointF mThumbPos = new PointF();
    private Mapper mMapper;
    private JoystickListener mListener;
    private Pos mJoystickPos = new Pos();
    @Shape
    private int mShape;
    private int mHalfCenter;
    private int mThumbRadius;
    private int mThumbPadding;
    private final static int SHAPE_RECT = 1;
    private final static int SHAPE_CIRCLE = 2;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({SHAPE_RECT, SHAPE_CIRCLE})
    public @interface Shape {
    }

    public interface JoystickListener {
        void onJoystickGrab(@NonNull JoystickView view);

        void onJoystickRelease(@NonNull JoystickView view);

        void onJoystickMove(@NonNull JoystickView view);
    }

    public void setMapper(@Nullable Mapper mapper) {
        mMapper = mapper;
        recalculateJoystickPos();
    }

    public void setListener(@Nullable JoystickListener listener) {
        mListener = listener;
    }

    public Pos getJoystickPos() {
        return mJoystickPos;
    }

    public boolean isJoystickGrabbed() {
        return mGrabbed;
    }

    public JoystickView(Context context) {
        this(context, null);
    }

    public JoystickView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public JoystickView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // Get attributes.
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.JoystickView, defStyleAttr, 0);
        //noinspection ResourceType
        mShape = a.getInt(R.styleable.JoystickView_joyview_shape, SHAPE_RECT);
        mHalfCenter = a.getDimensionPixelSize(R.styleable.JoystickView_joyview_halfCenter, (int) Util.dpToPixel(context, 30));
        mThumbRadius = a.getDimensionPixelSize(R.styleable.JoystickView_joyview_thumbRadius, (int) Util.dpToPixel(context, 20));
        int thumbStrokeWidth = a.getDimensionPixelSize(R.styleable.JoystickView_joyview_thumbStrokeWidth, (int) Util.dpToPixel(context, 0));
        mThumbPadding = a.getDimensionPixelSize(R.styleable.JoystickView_joyview_thumbPadding, (int) Util.dpToPixel(context, 4));
        int thumbColor = a.getColor(R.styleable.JoystickView_joyview_thumbColor, getContext().getResources().getColor(R.color.accent));
        a.recycle();
        // Setup the thumb paint.
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(thumbStrokeWidth);
        mPaint.setColor(thumbColor);
        mPaint.setStyle((thumbStrokeWidth == 0) ? Paint.Style.FILL : Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        joystickRelease();
        centerThumb(); // For initial centering.
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(mThumbPos.x, mThumbPos.y, mThumbRadius, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mThumbPos = projectInside(event.getX(), event.getY());
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                joystickGrab();
                break;
            case MotionEvent.ACTION_MOVE:
                joystickMove();
                break;
            case MotionEvent.ACTION_UP:
                joystickRelease();
                break;
            default:
                return false;
        }

        invalidate();
        return true;
    }

    private void joystickGrab() {
        if (!mGrabbed) {
            mGrabbed = true;
            recalculateJoystickPos();
            if (mListener != null) {
                mListener.onJoystickGrab(this);
            }
        }
    }

    private void joystickMove() {
        // Note that this does not grab.
        // If we lost grab is somehow, we have to release first to be able to grab again.
        // The grab must happen inside the view - we cannot grab the joystick by "sliding in".
        if (recalculateJoystickPos()) {
            if (mListener != null) {
                mListener.onJoystickMove(this);
            }
        }
    }

    private void joystickRelease() {
        if (mGrabbed) {
            mGrabbed = false;
            centerThumb();
            recalculateJoystickPos();
            if (mListener != null) {
                mListener.onJoystickRelease(this);
            }
        }
    }

    private void centerThumb() {
        Rect rect = new Rect();
        getLocalVisibleRect(rect);
        mThumbPos = new PointF(rect.exactCenterX(), rect.exactCenterY());
    }

    private boolean recalculateJoystickPos() {
        Pos joystickPos;
        if (mMapper != null && mGrabbed) {
            Rect rect = new Rect();
            getLocalVisibleRect(rect);
            rect.inset(mThumbRadius + mThumbPadding, mThumbRadius + mThumbPadding); // We stay inside this rect.
            joystickPos = mMapper.mapThumbPos(mThumbPos, rect, mHalfCenter);
        } else {
            joystickPos = new Pos();
        }
        if (!mJoystickPos.equals(joystickPos)) {
            mJoystickPos = joystickPos;
            return true;
        }
        return false;
    }

    private PointF projectInside(float x, float y) {
        Rect rect = new Rect();
        getLocalVisibleRect(rect);
        rect.inset(mThumbRadius + mThumbPadding, mThumbRadius + mThumbPadding); // We stay inside this rect.
        int halfWidth = rect.width() / 2;
        int halfHeight = rect.height() / 2;
        PointF center = new PointF(rect.exactCenterX(), rect.exactCenterY());
        float rX = x - center.x; // Coordinate relative to center.
        float rY = y - center.y; // Coordinate relative to center.
        // Determine the center-border ratio.
        // 0== dead center 1== on the border >1 outside
        float cbRatio = 0;
        if (mShape == SHAPE_RECT) {
            // Compare length to circle radius.
            float len = (float) Math.sqrt(rX * rX + rY * rY);
            cbRatio = len / halfWidth;
        } else {
            // Compare x and y coordinates to the appropriate edges separately.
            float cbX = Math.abs(rX / halfWidth);
            float cbY = Math.abs(rY / halfHeight);
            cbRatio = Math.max(cbX, cbY);
        }
        if (cbRatio > 1.0) {
            // If we are outside the limit, pull in to be just on the border.
            return new PointF(center.x + rX / cbRatio, center.y + rY / cbRatio);
        } else {
            // Already inside.
            return new PointF(x, y);
        }
    }

    /**
     * Joystick position - a two dimensional coordinate.
     * Coordinate values depend on the Mapper.
     */
    public static class Pos {
        public int mX;
        public int mY;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pos pos = (Pos) o;

            if (mX != pos.mX) return false;
            return mY == pos.mY;

        }

        @Override
        public int hashCode() {
            int result = mX;
            result = 31 * result + mY;
            return result;
        }
    }

    /**
     * Joystick thumb position mapper. Maps thumb position (the touch point inside the joystick
     * view rectangle) to a joystick coordinate. Meaning of the resulting joystick coordinates is
     * defined by the concrete mapper implementation.
     */
    public interface Mapper {
        /**
         * Map thumb position to joystick position.
         *
         * @param point      Thumb position.
         * @param rect       Joytick rect.
         * @param halfCenter Steering deadzone half size.
         * @return Mapped position.
         */
        Pos mapThumbPos(PointF point, Rect rect, int halfCenter);
    }

    /**
     * 3x3 digital joystick, 9 discrete positions.
     * X is the horizontal movement, 100==forward, 0==stop, -100==backward
     * Y is the vertical movement, 100==right, 0==stop, -100==left
     */
    public static class Mapper3x3 implements Mapper {

        @Override
        public Pos mapThumbPos(PointF point, Rect rect, int halfCenter) {
            Pos pos = new Pos();
            if (point.x > rect.centerX() + halfCenter) {
                pos.mX = 100;
            } else if (point.x < rect.centerX() - halfCenter) {
                pos.mX = -100;
            }
            if (point.y > rect.centerY() + halfCenter) {
                pos.mY = -100;
            } else if (point.y < rect.centerY() - halfCenter) {
                pos.mY = 100;
            }
            return pos;
        }
    }

    /**
     * Analog joystick, 0-100 speed and steering in 10 discrete steps (plus stop) both directions.
     * X is the horizontal movement, 10...100==forward, 0==stop, -100...-10==backward
     * Y is the vertical movement, 10...100==right, 0==stop, -100...-10==left
     */
    public static class Mapper21x21 implements Mapper {

        @Override
        public Pos mapThumbPos(PointF point, Rect rect, int halfCenter) {
            Pos pos = new Pos();
            if (point.x > rect.centerX() + halfCenter) {
                pos.mX = (int) (point.x - rect.centerX() - halfCenter) * 100 / (rect.width() / 2 - halfCenter);
            } else if (point.x < rect.centerX() - halfCenter) {
                pos.mX = -(int) ((rect.centerX() - point.x - halfCenter) * 100 / (rect.width() / 2 - halfCenter));
            }
            if (point.y > rect.centerY() + halfCenter) {
                pos.mY = -(int) ((point.y - rect.centerY() - halfCenter) * 100 / (rect.height() / 2 - halfCenter));
            } else if (point.y < rect.centerY() - halfCenter) {
                pos.mY = (int) (rect.centerY() - point.y - halfCenter) * 100 / (rect.height() / 2 - halfCenter);
            }
            // Round down to multiple of 10
            pos.mX = pos.mX / 10 * 10;
            pos.mY = pos.mY / 10 * 10;
            return pos;
        }
    }

    /**
     * Analog joystick, length and degrees angle.
     * X is the length, 0-100 in 10 discrete steps.
     * Y is the angle, 0..+359 in 24 discrete steps (15 degrees resolution)
     */
    public static class MapperLengthAngle implements Mapper {

        @Override
        public Pos mapThumbPos(PointF point, Rect rect, int halfCenter) {
            PointF center = new PointF(rect.exactCenterX(), rect.exactCenterY());
            float x = point.x - center.x;
            float y = point.y - center.y;
            float len = (float) Math.sqrt(x * x + y * y);
            float angle = 0.f;
            Pos pos = new Pos();
            if (len > halfCenter) {
                len = (len - halfCenter) * 100.f / (rect.width() / 2.f - halfCenter) + 0.5f; // Adjust for deadzone.
                angle = (float) (Math.atan2(y, -x) * 180.f / Math.PI + 180.f);
            } else {
                len = 0.f; // Adjust for deadzone.
            }
            // Round down to multiple of 10
            pos.mX = ((int) len) / 10 * 10;
            pos.mY = ((int) angle) / 15 * 15;
            return pos;
        }
    }

}
