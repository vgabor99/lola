package com.vargag99.lola.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.net.HostAndPort;
import com.vargag99.lola.R;
import com.vargag99.lola.generic.Util;
import com.vargag99.lola.session.CameraConfig;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Preferences.
 * Note, not all preferences are visible to the user on the preferences screen.
 */
public class Preferences {
    private final Context mContext;
    private final SharedPreferences mPrefs;
    private static final Pattern RESOLUTION_PATTERN = Pattern.compile("(\\d+)x(\\d+)");

    public Preferences(Context context) {
        mContext = context;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void setDefaults() {
        PreferenceManager.setDefaultValues(mContext, R.xml.preferences, false);
    }

    @NonNull
    public HostAndPort getAddress() {
        String address = mPrefs.getString(mContext.getString(R.string.pref_key_address), null);
        return HostAndPort.fromString(address);
    }

    @Nullable
    public CameraConfig getCameraConfig() {
        String id = mPrefs.getString(mContext.getString(R.string.pref_key_camera_config), null);
        return Util.findConfig(getCameraConfigs(), id);
    }

    public void setCameraConfigs(@NonNull List<CameraConfig> cameraConfigs) {
        // Set camera configs received from the server.
        // If the user setting is not in the received list, clear the user setting.
        // The is done in a transaction, the setting value is always either valid or unset.
        String current = mPrefs.getString(mContext.getString(R.string.pref_key_camera_config), null);
        boolean currentIsValid = Util.findConfig(cameraConfigs, current) != null;
        SharedPreferences.Editor edit = mPrefs.edit();
        edit.putString(mContext.getString(R.string.pref_key_camera_config_data), Util.toJson(cameraConfigs));
        if (!currentIsValid) {
            // Remove old invalid setting.
            edit.remove(mContext.getString(R.string.pref_key_camera_config));
            // Set a default if we can - use the first from the server list.
            if (cameraConfigs.size() > 0) {
                edit.putString(mContext.getString(R.string.pref_key_camera_config), cameraConfigs.get(0).id);
            }
        }
        edit.apply();
    }

    @NonNull
    public List<CameraConfig> getCameraConfigs() {
        // Local copy of all available camera configs, as received from the server.
        // Not a user setting, but it is needed to display the list and sanitize the user setting.
        String json = mPrefs.getString(mContext.getString(R.string.pref_key_camera_config_data), "[]");
        return Util.fromJson(json);
    }

    public boolean isCameraEnabled() {
        // This user setting is show on the Toolbar as a toggle button.
        return mPrefs.getBoolean(mContext.getString(R.string.pref_key_camera_enabled), true);
    }

    public void setCameraEnabled(boolean enabled) {
        // This user setting is show on the Toolbar as a toggle button.
        mPrefs.edit()
                .putBoolean(mContext.getString(R.string.pref_key_camera_enabled), enabled)
                .apply();
    }

    public boolean isHeadlightsEnabled() {
        // This user setting is shown on the Toolbar as a toggle button.
        return mPrefs.getBoolean(mContext.getString(R.string.pref_key_headlights_enabled), false);
    }

    public void setHeadlightsEnabled(boolean enabled) {
        // This user setting is show on the Toolbar as a toggle button.
        mPrefs.edit()
                .putBoolean(mContext.getString(R.string.pref_key_headlights_enabled), enabled)
                .apply();
    }

    public int getVibrateMillis() {
        return mPrefs.getInt(mContext.getString(R.string.pref_key_vibrate_millis), 0);
    }

    public boolean touchHereEnabled() {
        // This user setting is shown on the "Touch here to drive" as a checkbox.
        return mPrefs.getBoolean(mContext.getString(R.string.pref_key_touch_here_enabled), true);
    }

    public void setTouchHereEnabled(boolean enabled) {
        // This user setting is shown on the "Touch here to drive" as a checkbox.
        mPrefs.edit()
                .putBoolean(mContext.getString(R.string.pref_key_touch_here_enabled), enabled)
                .apply();
    }

}
