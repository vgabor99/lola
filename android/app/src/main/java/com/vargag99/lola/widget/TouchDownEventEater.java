package com.vargag99.lola.widget;

import android.view.MotionEvent;
import android.view.View;

/**
 * Eat touch down events (not interfering with other events).
 */
final class TouchDownEventEater implements View.OnTouchListener {

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            return true; // Eat the MotionEvent.ACTION_DOWN.
        }
        return false; // Keep other events - allow ACTION_MOVE, ACTION_CANCEL and ACTION_UP.
    }
}
