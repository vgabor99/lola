package com.vargag99.lola.generic;

/**
 * Interface to offer handling of back button press. (Chain of Responsibility pattern.)
 */
public interface BackHandler {
    boolean onBackPressed();
}