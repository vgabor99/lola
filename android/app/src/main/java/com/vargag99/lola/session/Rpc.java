package com.vargag99.lola.session;

import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.net.HostAndPort;
import com.google.gson.Gson;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.vargag99.lola.generic.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * PI RPC functions.
 */
public class Rpc {
    private final static String TAG = Rpc.class.getSimpleName();
    private final ClientTransport mTransport;
    private int mId = 1; // Request id, running number.

    public Rpc(ClientTransport transport) {
        this.mTransport = transport;
    }

    public String connect(long when) throws Exception {
        JSONRPC2Request request = request("connect")
                .param("when", when)
                .build();
        return call(request).toString();
    }

    public void close(String sid) throws Exception {
        JSONRPC2Request request = request("close")
                .param("sid", sid)
                .build();
        call(request);
    }

    public SysInfo ping(String sid, long when) throws Exception {
        JSONRPC2Request request = request("ping")
                .param("sid", sid)
                .param("when", when)
                .build();
        return new Gson().fromJson(call(request).toString(), SysInfo.class);
    }

    // motor: -100 (backwards) ... 100 (forwards), inclusive
    // steer: -100 (left) ... 100 (right), inclusive
    public void driveMotorSteer(String sid, long when, int motor, int steer) throws Exception {
        JSONRPC2Request request = request("driveMotorSteer")
                .param("sid", sid)
                .param("when", when)
                .param("motor", motor)
                .param("steer", steer)
                .build();
        call(request);
    }

    // speed: 0 ... 100, inclusive
    // angle: 0 ... 259, inclusive
    public void driveSpeedAngle(String sid, long when, int speed, int angle) throws Exception {
        JSONRPC2Request request = request("driveSpeedAngle")
                .param("sid", sid)
                .param("when", when)
                .param("speed", speed)
                .param("angle", angle)
                .build();
        call(request);
    }

    // left: -100 (backwards) ... 100 (forwards), inclusive
    // right: -100 (backwards) ... 100 (forwards), inclusive
    public void driveTracks(String sid, long when, int left, int right) throws Exception {
        JSONRPC2Request request = request("driveTracks")
                .param("sid", sid)
                .param("when", when)
                .param("left", left)
                .param("right", right)
                .build();
        call(request);
    }

    public void headlights(String sid, long when, boolean on) throws Exception {
        JSONRPC2Request request = request("headlights")
                .param("sid", sid)
                .param("when", when)
                .param("on", on)
                .build();
        call(request);
    }

    public List<CameraConfig> getCameraConfigurations(String sid) throws Exception {
        // TODO remove SID, it's pointless on plain queries like this.
        JSONRPC2Request request = request("getCameraConfigurations")
                .param("sid", sid)
                .build();
        return Util.fromJson(call(request).toString());
    }

    @Nullable
    public HostAndPort startCamera(String sid, long when, String id, @Nullable HostAndPort clientAddress) throws Exception {
        JSONRPC2Request request = request("startCamera")
                .param("sid", sid)
                .param("when", when)
                .param("id", id)
                .param("clientAddress", (clientAddress == null) ? null : clientAddress.toString())
                .build();
        Object result = call(request);
        return (result == null) ? null : HostAndPort.fromString(result.toString());
    }

    public void stopCamera(String sid) throws Exception {
        JSONRPC2Request request = request("stopCamera")
                .param("sid", sid)
                .build();
        call(request);
    }

    // Convenience function to initialize builder.
    private JSONRPC2RequestBuilder request(String method) {
        return new JSONRPC2RequestBuilder(method, mId++);
    }

    // Convenience request builder.
    private static class JSONRPC2RequestBuilder {
        private final String mMethod;
        private final int mId;
        private Map<String, Object> mParams = new HashMap<>();

        public JSONRPC2RequestBuilder(String method, int id) {
            mMethod = method;
            mId = id;
        }

        JSONRPC2RequestBuilder param(String name, String value) {
            mParams.put(name, value);
            return this;
        }

        JSONRPC2RequestBuilder param(String name, long value) {
            mParams.put(name, value);
            return this;
        }

        JSONRPC2RequestBuilder param(String name, int value) {
            mParams.put(name, value);
            return this;
        }

        JSONRPC2RequestBuilder param(String name, boolean value) {
            mParams.put(name, value);
            return this;
        }

        JSONRPC2Request build() {
            if (mParams.isEmpty()) {
                return new JSONRPC2Request(mMethod, mId);
            } else {
                return new JSONRPC2Request(mMethod, mParams, mId);
            }
        }
    }

    // Convenience function to call RPC and return the response payload (or throw exception).
    private Object call(JSONRPC2Request request) throws Exception {
        try {
            String requestStr = request.toJSONString();
            Log.d(TAG, ">> " + requestStr);
            mTransport.send(requestStr);
            JSONRPC2Response response;
            for (; ; ) {
                String responseStr = mTransport.receive();
                Log.d(TAG, "<< " + responseStr);
                response = JSONRPC2Response.parse(responseStr);
                if (((Number) response.getID()).longValue() == ((Number) request.getID()).longValue()) {
                    break;
                }
                Log.w(TAG, "Stray response");
            }
            if (response.indicatesSuccess()) {
                return response.getResult();
            } else {
                throw response.getError();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            throw e;
        }
    }
}
