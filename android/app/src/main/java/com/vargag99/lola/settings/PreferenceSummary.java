package com.vargag99.lola.settings;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceManager;
import android.text.TextUtils;

import com.vargag99.lola.R;

import net.xpece.android.support.preference.ListPreference;
import net.xpece.android.support.preference.MultiSelectListPreference;
import net.xpece.android.support.preference.RingtonePreference;
import net.xpece.android.support.preference.SeekBarDialogPreference;
import net.xpece.android.support.preference.SeekBarPreference;
import net.xpece.android.support.preference.SharedPreferencesCompat;

import java.util.HashSet;

/**
 * Utility methods to set summary for various types of preferences.
 */
public class PreferenceSummary {

    /**
     * Get the preference summary for various types of preferences.
     *
     * @param preference Preference.
     * @param value      Value.
     * @return Summary string.
     */
    public static String getSummary(@NonNull Preference preference, @NonNull Object value) {
        Context context = preference.getContext();
        String stringValue = value.toString();

        if (preference instanceof ListPreference) {
            // For list preferences, look up the correct display value in
            // the preference's 'entries' list.
            ListPreference listPreference = (ListPreference) preference;
            int index = listPreference.findIndexOfValue(stringValue);
            return (index >= 0)
                    ? listPreference.getEntries()[index].toString()
                    : null;
        } else if (preference instanceof MultiSelectListPreference) {
            return stringValue.trim().substring(1, stringValue.length() - 1); // strip []
        } else if (preference instanceof RingtonePreference) {
            // For ringtone preferences, look up the correct display value  using RingtoneManager.
            if (TextUtils.isEmpty(stringValue)) {
                // Empty values correspond to 'silent' (no ringtone).
                return context.getString(R.string.pref_ringtone_silent);

            } else {
                Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse(stringValue));
                if (ringtone == null) {
                    // Clear the summary if there was a lookup error.
                    return null;
                } else {
                    // Set the summary to reflect the new ringtone display  name.
                    return ringtone.getTitle(context);
                }
            }
        } else {
            // For all other preferences, set the summary to the value's  simple string representation.
            return stringValue;
        }
    }

    /**
     * Return current value as string. Used to set current summary initially.
     *
     * @param preference Preference
     * @return Current value as string.
     */
    public static String getCurrentValueAsString(@NonNull Preference preference) {
        if (preference instanceof MultiSelectListPreference) {
            return SharedPreferencesCompat.getStringSet(
                    PreferenceManager.getDefaultSharedPreferences(preference.getContext()),
                    preference.getKey(),
                    new HashSet<String>())
                    .toString();
        } else if (preference instanceof SeekBarPreference || preference instanceof SeekBarDialogPreference) {
            return Integer.toString(PreferenceManager
                    .getDefaultSharedPreferences(preference.getContext())
                    .getInt(preference.getKey(), 0));
        } else {
            return PreferenceManager
                    .getDefaultSharedPreferences(preference.getContext())
                    .getString(preference.getKey(), "");
        }
    }

}
