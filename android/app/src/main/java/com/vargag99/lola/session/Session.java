package com.vargag99.lola.session;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Process;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.net.HostAndPort;
import com.vargag99.lola.generic.Callback;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Session, a connected remote.
 * Calls the RPC in a thread, event-based operation.
 * Provides keepalive ping.
 */
public class Session {
    private final static String TAG = Session.class.getSimpleName();
    @NonNull
    private final ClientTransport mTransport;
    @NonNull
    private final Rpc mRpc;
    @NonNull
    private Status mStatus = Status.NULL;
    @Nullable
    private Exception mException = null;
    private AtomicLong mCommandTime = new AtomicLong(0);
    private final StatusTracker mStatusTracker = new StatusTracker();
    private final SysInfoDelivery mSysInfoDelivery = new SysInfoDelivery();
    @Nullable
    private Listener mListener;
    private String mSid; // Use getSid() for proper error handling.
    private HandlerThread mWorkerThread;
    private Handler mWorkerHandler;
    private Handler mUiHandler;
    private final long PING_PERIOD_MILLIS = 500; // Ping twice per second.

    public Session(@NonNull ClientTransport transport, @NonNull Rpc rpc) {
        mTransport = transport;
        mRpc = rpc;
    }

    public enum Status {
        NULL, // Initial state.
        CONNECTING, // Connecting the session.
        CONNECTED, // Session is live.
        ERROR // Error.
    }

    public interface Listener {
        void onStatusChange(@NonNull Status status);

        void onSysInfo(@Nullable SysInfo sysInfo);
    }

    public void setListener(@Nullable Listener listener) {
        mListener = listener;
    }

    @NonNull
    public Status getStatus() {
        return mStatus;
    }

    @Nullable
    public Exception getException() {
        return mException;
    }

    public long getCommandTime() {
        return mCommandTime.get();
    }

    public void connect() {
        mUiHandler = new Handler();
        mWorkerThread = new HandlerThread("Session worker", Process.THREAD_PRIORITY_DEFAULT);
        mWorkerThread.start();
        mWorkerHandler = new Handler(mWorkerThread.getLooper());
        mWorkerHandler.post(new TimedCommand() {
            @Override
            public void doRun() {
                try {
                    mTransport.connect(); // Connect the transport.
                    mSid = mRpc.connect(mCreated); // Send the RPC connect message.
                    schedulePing(); // Start the keepalive.
                    postResult(mStatusTracker, Status.CONNECTED);
                } catch (Exception e) {
                    Log.e(TAG, "connect failed:", e);
                    postError(mStatusTracker, e);
                }
            }
        });
        postResult(mStatusTracker, Status.CONNECTING);
    }

    public void close() {
        mWorkerHandler.postAtFrontOfQueue(new Runnable() {
            @Override
            public void run() {
                if (mSid != null) {
                    try {
                        // Try graceful close, but ignore the outcome.
                        mRpc.close(mSid); // RPC close message.
                        mTransport.close(); // Close the transport.
                    } catch (Exception e) {
                        // Ignore.
                    } finally {
                        mSid = null;
                        mWorkerThread.quit();
                        postResult(mStatusTracker, Status.NULL);
                    }
                }
            }
        });
    }

    public void ping() {
        mWorkerHandler.post(new TimedCommand() {
            @Override
            public void doRun() {
                schedulePing();
                try {
                    SysInfo sysInfo = mRpc.ping(getSid(), mCreated);
                    postResult(mSysInfoDelivery, sysInfo);
                    postResult(mStatusTracker, Status.CONNECTED);
                } catch (Exception e) {
                    Log.e(TAG, "ping failed:", e);
                    postError(mStatusTracker, e);
                    postError(mSysInfoDelivery, e);
                }
            }
        });
    }

    public void driveMotorSteer(final int motor, final int steer) {
        mWorkerHandler.post(new TimedCommand() {
            @Override
            public void doRun() {
                try {
                    mRpc.driveMotorSteer(getSid(), mCreated, motor, steer);
                    postResult(mStatusTracker, Status.CONNECTED);
                } catch (Exception e) {
                    Log.e(TAG, "driveMotorSteer failed:", e);
                    postError(mStatusTracker, e);
                }
            }
        });
    }

    public void driveSpeedAngle(final int speed, final int angle) {
        mWorkerHandler.post(new TimedCommand() {
            @Override
            public void doRun() {
                try {
                    mRpc.driveSpeedAngle(getSid(), mCreated, speed, angle);
                    postResult(mStatusTracker, Status.CONNECTED);
                } catch (Exception e) {
                    Log.e(TAG, "driveSpeedAnglefinal failed:", e);
                    postError(mStatusTracker, e);
                }
            }
        });
    }

    public void driveTracks(final int left, final int right) {
        mWorkerHandler.post(new TimedCommand() {
            @Override
            public void doRun() {
                try {
                    mRpc.driveTracks(getSid(), mCreated, left, right);
                    postResult(mStatusTracker, Status.CONNECTED);
                } catch (Exception e) {
                    Log.e(TAG, "driveTracks failed:", e);
                    postError(mStatusTracker, e);
                }
            }
        });
    }

    public void headlights(final boolean on) {
        mWorkerHandler.post(new TimedCommand() {
            @Override
            public void doRun() {
                try {
                    mRpc.headlights(getSid(), mCreated, on);
                    postResult(mStatusTracker, Status.CONNECTED);
                } catch (Exception e) {
                    Log.e(TAG, "headlights failed:", e);
                    postError(mStatusTracker, e);
                }
            }
        });
    }

    public void getCameraConfigurations(@Nullable final Callback<List<CameraConfig>> callback) {
        mWorkerHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    List<CameraConfig> cameraConfig = mRpc.getCameraConfigurations(getSid());
                    postResult(mStatusTracker, Status.CONNECTED);
                    postResult(callback, cameraConfig);
                } catch (Exception e) {
                    Log.e(TAG, "getCameraConfigurations failed:", e);
                    postError(mStatusTracker, e);
                    postError(callback, e);
                }
            }
        });
    }

    public void startCamera(final String id,
                            final HostAndPort clientAddress,
                            @Nullable final Callback<HostAndPort> callback) {
        mWorkerHandler.post(new TimedCommand() {
            @Override
            public void doRun() {
                try {
                    HostAndPort serverAddress = mRpc.startCamera(getSid(), mCreated, id, clientAddress);
                    postResult(mStatusTracker, Status.CONNECTED);
                    postResult(callback, serverAddress);
                } catch (Exception e) {
                    Log.e(TAG, "startCamera failed:", e);
                    postError(mStatusTracker, e);
                    postError(callback, e);
                }
            }
        });
    }

    public void stopCamera() {
        mWorkerHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    mRpc.stopCamera(getSid());
                    postResult(mStatusTracker, Status.CONNECTED);
                } catch (Exception e) {
                    Log.e(TAG, "stopCamera failed:", e);
                    postError(mStatusTracker, e);
                }
            }
        });
    }

    private String getSid() throws Exception {
        if (mSid == null) {
            throw new SessionClosedException();
        }
        return mSid;
    }

    private void schedulePing() {
        if (mSid != null) {
            mWorkerHandler.removeCallbacks(mScheduledPing);
            mWorkerHandler.postDelayed(mScheduledPing, PING_PERIOD_MILLIS);
        }
    }

    private abstract class TimedCommand implements Runnable {
        protected final long mCreated = SystemClock.uptimeMillis();

        public abstract void doRun();

        @Override
        public void run() {
            long execStart = SystemClock.uptimeMillis();
            try {
                doRun();
            } finally {
                long execEnd = SystemClock.uptimeMillis();
                Log.d(TAG, String.format("Queued %d, executed %d", execStart - mCreated, execEnd - execStart));
                mCommandTime.set(execEnd - mCreated);
            }
        }
    }

    private final Runnable mScheduledPing = new Runnable() {
        @Override
        public void run() {
            try {
                ping();
            } catch (Exception e) {
                // Ignore.
            }
        }
    };

    private <T> void postResult(@Nullable Callback<T> callback, @Nullable T result) {
        if (callback != null) {
            mUiHandler.post(new Callback.Result(callback, result));
        }
    }

    private void postError(@Nullable Callback<?> callback, @NonNull Exception exception) {
        if (callback != null) {
            mUiHandler.post(new Callback.Error(callback, exception));
        }
    }

    /**
     * Status tracker. Delivers the session status to the listener.
     */
    private class StatusTracker implements Callback<Status> {

        @Override
        public void onResult(Status result) {
            mException = null;
            setStatus(result);
        }

        @Override
        public void onError(@NonNull Exception e) {
            if (mStatus == Status.ERROR && e instanceof SessionClosedException) {
                // We already have a session error; ignore SessionClosedExceptions
                // so they don't hide the real reason.
                return;
            }
            mException = e;
            setStatus(Status.ERROR);
        }

        private void setStatus(@NonNull Status status) {
            if (status != mStatus) {
                mStatus = status;
                if (mListener != null) {
                    mListener.onStatusChange(mStatus);
                }
            }
        }

    }

    /**
     * SysInfo delivery. Delivers the SysInfo to the listener.
     */
    private class SysInfoDelivery implements Callback<SysInfo> {

        @Override
        public void onResult(SysInfo result) {
            deliver(result);
        }

        @Override
        public void onError(@NonNull Exception e) {
            deliver(null);
        }

        private void deliver(@Nullable SysInfo sysInfo) {
            if (mListener != null) {
                mListener.onSysInfo(sysInfo);
            }
        }
    }

}