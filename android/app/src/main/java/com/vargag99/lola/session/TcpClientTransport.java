package com.vargag99.lola.session;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * TCP socket transport.
 * The socket is kept open (like in HTTP connection:keep-alive).
 * Each RPC message is one line; sending an empty line is the close signal.
 */
public class TcpClientTransport implements ClientTransport {
    private final static String TAG = TcpClientTransport.class.getSimpleName();
    public final String mHost;
    public final int mPort;
    private Socket mSocket;
    private DataOutputStream mOut;
    private BufferedReader mIn;

    public TcpClientTransport(String host, int port) {
        mHost = host;
        mPort = port;
        mSocket = new Socket();
    }

    @Override
    public void connect() throws Exception {
        Socket socket = null;
        DataOutputStream out = null;
        BufferedReader in = null;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(mHost, mPort), 2000);
            socket.setTcpNoDelay(true); // No socket level buffering.
            socket.setSoTimeout(1000);
            out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (Exception e) {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e1) {
                    Log.e(TAG, "Failed closing socket", e1);
                }
            }
            throw e;
        }
        mSocket = socket;
        mOut = out;
        mIn = in;
    }

    @Override
    public void close() {
        if (mSocket != null) {
            try {
                // Graceful close - send the close signal (empty string).
                mOut.writeByte('\n');
                mOut.flush();
                mSocket.close();
            } catch (Exception e) {
                Log.e(TAG, "Failed closing socket", e);
            }
            mSocket = null;
            mOut = null;
            mIn = null;
        }
    }

    @Override
    @NonNull
    public String call(@NonNull String request) throws Exception {
        send(request);
        return receive();
    }

    @Override
    public void send(@NonNull String request) throws Exception {
        mOut.writeBytes(request);
        mOut.writeByte('\n');
        mOut.flush();
    }

    @NonNull
    @Override
    public String receive() throws Exception {
        return mIn.readLine();
    }

}
