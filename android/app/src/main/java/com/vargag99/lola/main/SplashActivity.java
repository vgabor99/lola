package com.vargag99.lola.main;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.vargag99.lola.R;
import com.vargag99.lola.gstreamer.Player;

import io.fabric.sdk.android.Fabric;

/**
 * This shows splash screen until Gstreamer is initialized on a background thread.
 */
public class SplashActivity extends AppCompatActivity {
    private final static String TAG = SplashActivity.class.getSimpleName();
    private boolean mIsResumed;

    View mRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            // Pre-18, fallback to "sensorFallback" instead of "userLandscape"
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        if (Player.isInitDone()) {
            launchMain();
            return; // Already initialized.
        }
        setContentView(R.layout.splash);
        mRoot = findViewById(R.id.root);
        setGradientBackground();
        new InitGstreamer().execute();
    }

    private void setGradientBackground() {
        // Setting relative size to radial gradient is not working.
        // We have to set absolute radius size, this is not possible from XML.
        // Do it by hand. We use the display size because layout is not done yet.
        ShapeDrawable drawable = new ShapeDrawable(new RectShape());
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        float r = (float) Math.sqrt((float) (size.x * size.x + size.y * size.y)) / 2;
        drawable.getPaint().setShader(new RadialGradient(
                size.x / 2,
                size.y / 2,
                r,
                getResources().getColor(R.color.light_green_500),
                getResources().getColor(R.color.light_green_900),
                Shader.TileMode.CLAMP));
        mRoot.setBackground(drawable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsResumed = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsResumed = true;
    }

    private void launchMain() {
        startActivity(new Intent(this, MainActivity.class));
    }

    private class InitGstreamer extends AsyncTask<Void, Void, Exception> {
        @Override
        protected Exception doInBackground(Void... params) {
            try {
                Player.init(getApplicationContext());
                return null;
            } catch (Exception e) {
                Log.e(TAG, "Player.init failed", e);
                return e;
            }
        }

        @Override
        protected void onPostExecute(Exception e) {
            if (e != null) {
                throw new RuntimeException(e);
            }
            if (mIsResumed) {
                launchMain();
            }
        }
    }
}
