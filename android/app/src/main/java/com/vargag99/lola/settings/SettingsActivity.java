package com.vargag99.lola.settings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.vargag99.lola.R;

import net.xpece.android.support.preference.Fixes;

/**
 * Settings activity.
 */
public class SettingsActivity extends AppCompatActivity {

    Toolbar mToolbar;

    private SettingsFragment mSettingsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences);
        Fixes.updateLayoutInflaterFactory(getLayoutInflater());
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (savedInstanceState == null) {
            mSettingsFragment = SettingsFragment.newInstance("root_key");
            getSupportFragmentManager().beginTransaction().add(R.id.content, mSettingsFragment, "Settings").commit();
        } else {
            mSettingsFragment = (SettingsFragment) getSupportFragmentManager().findFragmentByTag("Settings");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setTitle(R.string.title_settings);
    }

    @Override
    public void onBackPressed() {
        if (mSettingsFragment.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

}
