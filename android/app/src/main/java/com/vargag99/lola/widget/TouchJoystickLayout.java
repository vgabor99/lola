package com.vargag99.lola.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.vargag99.lola.R;

/**
 * FrameLayout to provide "joystick at touch point" functionality.
 * Spies on touch events while still delivering them to children.
 * On touch down, if the position is appropriate for the joystick, shows the joystick.
 * On touch up, hides the joystick.
 */
public class TouchJoystickLayout extends RelativeLayout {
    private View mJoystick;
    private View mTouchHere;
    private boolean mTouchHereEnabled = true;
    private boolean mTouched;
    private final static TouchDownEventEater mTouchDownEventEater = new TouchDownEventEater();

    public TouchJoystickLayout(Context context) {
        super(context);
    }

    public TouchJoystickLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TouchJoystickLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setTouchHereEnabled(boolean enabled, boolean animate) {
        mTouchHereEnabled = enabled;
        updateTouchHere(animate);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mJoystick = findViewById(R.id.joystick);
        mJoystick.setAlpha(0.f);
        mTouchHere = findViewById(R.id.touch_here);
        mTouchHere.setAlpha(mTouchHereEnabled ? 1.f : 0.f);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // If the touch point allows putting down the joystick without being cropped,
                // do it.
                Rect joyRect = new Rect(0, 0, mJoystick.getWidth(), mJoystick.getHeight()); // Joystick size.
                joyRect.offset(-joyRect.width() / 2, -joyRect.height() / 2); // Center on touch.
                joyRect.offset((int) event.getX(), (int) event.getY()); // Target joystick rect.
                Rect myRect = new Rect(0, 0, getWidth(), getHeight());
                if (myRect.contains(joyRect)) {
                    mJoystick.setTranslationX(joyRect.left);
                    mJoystick.setTranslationY(joyRect.top);
                    mTouched = true;
                    updateJoystick(true);
                    updateTouchHere(true);
                }
                break;
            case MotionEvent.ACTION_UP:
                mTouched = false;
                updateJoystick(true);
                updateTouchHere(true);
                break;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_CANCEL:
            default:
                break;
        }
        return false;
    }

    private void updateJoystick(boolean animate) {
        mJoystick.clearAnimation();
        boolean show = mTouched;
        float alpha = show ? 1.f : 0.f;
        if (animate) {
            mJoystick.animate()
                    .alpha(alpha)
                    .setStartDelay(0)
                    .setDuration(mJoystick.getResources().getInteger(android.R.integer.config_shortAnimTime))
                    .start();
        } else {
            mJoystick.setAlpha(alpha);
        }
        // When the joystick gets hidden, we prevent it getting ACTION_DOWN events.
        mJoystick.setOnTouchListener(show ? null : mTouchDownEventEater);
    }

    private void updateTouchHere(boolean animate) {
        mTouchHere.clearAnimation();
        boolean show = mTouchHereEnabled && !mTouched;
        float alpha = show ? 1.f : 0.f;
        if (animate) {
            mTouchHere.animate()
                    .alpha(alpha)
                    .setStartDelay(show ? 2000 : 0)
                    .setDuration(show ?
                            mJoystick.getResources().getInteger(android.R.integer.config_longAnimTime) :
                            mJoystick.getResources().getInteger(android.R.integer.config_shortAnimTime))
                    .start();
        } else {
            mTouchHere.setAlpha(alpha);
        }
    }

}
