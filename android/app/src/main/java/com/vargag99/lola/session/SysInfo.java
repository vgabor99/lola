package com.vargag99.lola.session;

import android.support.annotation.Keep;

/**
 * SysInfo as returned by "ping" command.
 */
@Keep
public class SysInfo {
    public int cpuLoad; // 0-100
    public float temp; // Temperature in Celsius.
    public int memTotal; // Total memory kbytes.
    public int memUsed; // Used memory kbytes.
    public int memFree; // Free memory kbytes.
    public float batteryVoltage; // Battery voltage in Volts.
}
