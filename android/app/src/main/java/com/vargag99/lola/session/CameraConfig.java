package com.vargag99.lola.session;

import android.support.annotation.Keep;

/**
 * Camera config as returned by the "getCameraConfigurations" command.
 */
@Keep
public class CameraConfig {
    public String id; // Stable id.
    public String description; // Human-readable description.
    public int width; // Width.
    public int height; // Height.
    public boolean connectionOriented; // Connection oriented or connectionless mode?
    public String serverCmd; // Useless in client side.
    public String clientConfig; // Pipeline config.

}
