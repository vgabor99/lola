package com.vargag99.lola.widget;

import android.animation.Animator;
import android.support.annotation.NonNull;
import android.view.View;

/**
 * Animated visibility change.
 * Switch between View.INVISIBLE and View.INVISIBLE states, using an alpha animation in between.
 * The animation is optional.
 */
public class ViewVisibilityAnimator {
    @NonNull
    private final View mView;

    public ViewVisibilityAnimator(@NonNull View view) {
        mView = view;
    }

    public void hide(boolean animate) {
        cancel();
        if (animate) {
            mView.animate()
                    .alpha(0.0f)
                    .setDuration(mView.getResources().getInteger(android.R.integer.config_shortAnimTime))
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mView.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    })
                    .start();
        } else {
            mView.setAlpha(0.0f);
            mView.setVisibility(View.INVISIBLE);
        }
    }

    public void show(boolean animate) {
        cancel();
        if (animate) {
            mView.animate()
                    .alpha(1.0f)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            mView.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    })
                    .setDuration(mView.getResources().getInteger(android.R.integer.config_shortAnimTime))
                    .start();
        } else {
            mView.setAlpha(1.0f);
            mView.setVisibility(View.VISIBLE);
        }
    }

    public void cancel() {
        mView.clearAnimation();
    }
}
