package com.vargag99.lola.main;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.common.net.HostAndPort;
import com.vargag99.lola.R;
import com.vargag99.lola.generic.Callback;
import com.vargag99.lola.generic.Vibrate;
import com.vargag99.lola.gstreamer.GstCameraFeed;
import com.vargag99.lola.session.CameraConfig;
import com.vargag99.lola.session.ClientTransport;
import com.vargag99.lola.session.Rpc;
import com.vargag99.lola.session.Session;
import com.vargag99.lola.session.SysInfo;
import com.vargag99.lola.session.TcpClientTransport;
import com.vargag99.lola.settings.Preferences;
import com.vargag99.lola.settings.SettingsActivity;
import com.vargag99.lola.widget.FixedAspectRatioFrameLayout;
import com.vargag99.lola.widget.GaugeView;
import com.vargag99.lola.widget.ImmersiveMode;
import com.vargag99.lola.widget.JoystickView;
import com.vargag99.lola.widget.TouchJoystickLayout;
import com.vargag99.lola.widget.ViewVisibilityAnimator;

import java.util.List;

/**
 * Main activity.
 * Shows the camera feed window and the joystick.
 * Connects and disconnects automatically.
 */
public class MainActivity extends AppCompatActivity {
    private final static String TAG = MainActivity.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    @Nullable
    View mTopLeft;
    @Nullable
    View mTopRight;
    @Nullable
    FixedAspectRatioFrameLayout mSurfaceContainer;
    SurfaceView mSurfaceView;
    View mSurfaceCurtain;
    CheckBox mTouchHereDisabled;
    TouchJoystickLayout mTouchJoystickLayout;
    JoystickView mJoystickView;
    ImageView mCameraButton;
    ImageView mHeadlightsButton;
    ImageView mSettingsButton;
    GaugeView mRpcGauge;
    GaugeView mBatteryGauge;
    ImageView mSessionStatus;

    private Vibrate mVibrate;
    private Preferences mPreferences;
    private Session mSession;
    private GstCameraFeed mCameraFeed;
    private Toast mToast;
    private ImmersiveMode mImmersiveMode;
    private ViewVisibilityAnimator mCurtainAnimator;
    private Icons mIcons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            // Pre-18, fallback to "sensorFallback" instead of "userLandscape"
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            // Pre-19, no immersive mode - fallback to fullscreen.
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mTopLeft = findViewById(R.id.top_left);
        mTopRight = findViewById(R.id.top_right);
        mSurfaceContainer = (FixedAspectRatioFrameLayout) findViewById(R.id.surface_container);
        mSurfaceView = (SurfaceView) findViewById(R.id.surface);
        mSurfaceCurtain = findViewById(R.id.surface_curtain);
        mTouchHereDisabled = (CheckBox) findViewById(R.id.touch_here_disabled);
        mTouchJoystickLayout = (TouchJoystickLayout) findViewById(R.id.touch_joystick_layout);
        mJoystickView = (JoystickView) findViewById(R.id.joystick);
        mCameraButton = (ImageView) findViewById(R.id.camera);
        mHeadlightsButton = (ImageView) findViewById(R.id.headlights);
        mSettingsButton = (ImageView) findViewById(R.id.settings);
        mRpcGauge = (GaugeView) findViewById(R.id.rpc_gauge);
        mBatteryGauge = (GaugeView) findViewById(R.id.battery_gauge);
        mSessionStatus = (ImageView) findViewById(R.id.session_status);

        mIcons = new Icons();
        mJoystickView.setListener(new JoystickSpeedAngleDriver());
        mPreferences = new Preferences(this);
        mPreferences.setDefaults();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 19 or more - immersive mode available
            mImmersiveMode = new ImmersiveMode(getWindow().getDecorView(), mTopLeft, mTopRight);
        }
        mCurtainAnimator = new ViewVisibilityAnimator(mSurfaceCurtain);
        mSessionStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reconnectRemote();
            }
        });
        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleCamera();
            }
        });
        mHeadlightsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleHeadlights();
            }
        });
        mSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSettings();
            }
        });
        mTouchHereDisabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPreferences.setTouchHereEnabled(!isChecked);
                mTouchJoystickLayout.setTouchHereEnabled(mPreferences.touchHereEnabled(), true);
            }
        });
        mTouchJoystickLayout.setTouchHereEnabled(mPreferences.touchHereEnabled(), false);
        updateHeadlightsButton();
    }

    private void toggleCamera() {
        boolean enabled = mPreferences.isCameraEnabled();
        enabled = !enabled;
        mPreferences.setCameraEnabled(enabled);
        if (mCameraFeed != null) {
            mCameraFeed.setEnabled(enabled);
        }
    }

    private void toggleHeadlights() {
        boolean enabled = mPreferences.isHeadlightsEnabled();
        enabled = !enabled;
        mPreferences.setHeadlightsEnabled(enabled);
        if (mSession != null) {
            mSession.headlights(enabled);
        }
        updateHeadlightsButton();
    }

    private void launchSettings() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private void updateHeadlightsButton() {
        mHeadlightsButton.setImageResource(mPreferences.isHeadlightsEnabled() ?
                R.drawable.ic_headlights_white_24dp :
                R.drawable.ic_headlights_off_white_24dp);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        disconnectRemote(); // Disconnect here too, onWindowFocusChanged(false) is not always called.
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVibrate = new Vibrate(this, mPreferences.getVibrateMillis());

//        if (mImmersiveMode != null) {
//            mSurfaceView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mImmersiveMode.showSystemBars();
//                    mImmersiveMode.delayedHideSystemBars();
//                }
//            });
//        }

        mJoystickView.setMapper(new JoystickView.MapperLengthAngle());
    }


    private class CameraConfigurationCallback implements Callback<List<CameraConfig>> {

        @Override
        public void onResult(List<CameraConfig> result) {
            mPreferences.setCameraConfigs(result);
            applyCameraConfig(mPreferences.getCameraConfig());
        }

        @Override
        public void onError(@NonNull Exception e) {
        }
    }

    private void applyCameraConfig(CameraConfig cameraConfig) {
        if (cameraConfig != null) {
            if (mSurfaceContainer != null) {
                // Adjust aspect ratio as needed.
                mSurfaceContainer.setAspectRatio((float) cameraConfig.width / (float) cameraConfig.height);
            }
            if (mCameraFeed != null) {
                mCameraFeed.setCameraConfig(cameraConfig);
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (mImmersiveMode != null && hasFocus) {
            mImmersiveMode.hideSystemBars();
        }
        // The remote disconnects immediately when nobody is using it.
        // This cannot be done with onResume/onPause. For example, onResume is called under screen lock.
        if (hasFocus) {
            connectRemote();
        } else {
            disconnectRemote();
        }
    }

    private void connectRemote() {
        Log.d(TAG, "connectRemote");
        if (mSession != null || mCameraFeed != null) {
            throw new IllegalStateException("Already connected");
        }
        HostAndPort address = mPreferences.getAddress();
        ClientTransport transport = new TcpClientTransport(address.getHostText(), address.getPort());
        Rpc rpc = new Rpc(transport);
        mSession = new Session(transport, rpc);
        mSession.setListener(new SessionReport());
        mSession.connect();
        mSession.headlights(mPreferences.isHeadlightsEnabled());
        mSession.getCameraConfigurations(new CameraConfigurationCallback()); // Refresh camera configuration.

        mCameraFeed = new GstCameraFeed(mSession);
        mCameraFeed.setSurfaceHolder(mSurfaceView.getHolder());
        mCameraFeed.setListener(new CameraFeedReport());
        mCameraFeed.setEnabled(mPreferences.isCameraEnabled());

        applyCameraConfig(mPreferences.getCameraConfig());

        updateStatusIcons();
    }

    private void disconnectRemote() {
        Log.d(TAG, "disconnectRemote");
        if (mCameraFeed != null) {
            mCameraFeed.setSurfaceHolder(null);
            mCameraFeed.setListener(null);
            mCameraFeed = null;
        }
        if (mSession != null) {
            mSession.setListener(null);
            mSession.close();
            mSession = null;
        }
    }

    private void reconnectRemote() {
        if (mSession == null || mSession.getStatus().equals(Session.Status.ERROR)) {
            disconnectRemote();
            connectRemote();
        }
    }

    /**
     * Drive with the motor-steer (rectangular) joystick.
     */
    private class JoystickMotorSpeedDriver implements JoystickView.JoystickListener {
        @Override
        public void onJoystickGrab(@NonNull JoystickView view) {
            drive(view.getJoystickPos());
        }

        @Override
        public void onJoystickRelease(@NonNull JoystickView view) {
            drive(view.getJoystickPos());
        }

        @Override
        public void onJoystickMove(@NonNull JoystickView view) {
            drive(view.getJoystickPos());
        }

        private void drive(JoystickView.Pos joystickPos) {
            if (mSession != null) {
                mVibrate.vibrate();
                mSession.driveMotorSteer(joystickPos.mY, joystickPos.mX);
            }

        }

    }

    /**
     * Drive with speed-angle (round) the joystick.
     */
    private class JoystickSpeedAngleDriver implements JoystickView.JoystickListener {
        @Override
        public void onJoystickGrab(@NonNull JoystickView view) {
            drive(view.getJoystickPos());
        }

        @Override
        public void onJoystickRelease(@NonNull JoystickView view) {
            drive(view.getJoystickPos());
        }

        @Override
        public void onJoystickMove(@NonNull JoystickView view) {
            drive(view.getJoystickPos());
        }

        private void drive(JoystickView.Pos joystickPos) {
            if (mSession != null) {
                mVibrate.vibrate();
                mSession.driveSpeedAngle(joystickPos.mX, joystickPos.mY);
            }

        }

    }

    /**
     * Provide feedback of the session.
     */
    private class SessionReport implements Session.Listener {

        @Override
        public void onStatusChange(@NonNull Session.Status status) {
            updateStatusIcons();
        }

        @Override
        public void onSysInfo(@Nullable SysInfo sysInfo) {
            updateSysInfo(sysInfo);
        }

    }

    /**
     * Provide feedback from GStreamer player.
     */
    private class CameraFeedReport implements GstCameraFeed.Listener {

        @Override
        public void onStateChanged(@NonNull GstCameraFeed cameraFeed, @NonNull GstCameraFeed.State state) {
            if (state.equals(GstCameraFeed.State.PLAYING)) {
                mCurtainAnimator.hide(false); // Hide the curtain without animation - the player does fade-in too.
            } else {
                mCurtainAnimator.show(true); // Show the curtain with animation.
            }
            updateStatusIcons();
        }
    }

    private void updateStatusIcons() {
        mSessionStatus.setImageResource(mIcons.getSessionStatusIcon(mSession));
        mCameraButton.setImageResource(mIcons.getCameraFeedIcon(mCameraFeed));
    }

    private void updateSysInfo(@Nullable SysInfo sysInfo) {
        if (sysInfo != null) {
            // Battery gauge is scaled up 100x
            mBatteryGauge.setValue((int) (sysInfo.batteryVoltage * 100.f));
        }
        // RPC gauge is reversed (small values are good), we have to reverse the values too.
        long rpcMillis = mSession.getCommandTime();
        mRpcGauge.setValue((int) (-10 * rpcMillis));
    }
}
