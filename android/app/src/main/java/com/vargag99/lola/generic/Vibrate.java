package com.vargag99.lola.generic;

import android.content.Context;
import android.os.Vibrator;

/**
 * Vibrator.
 */
public class Vibrate {
    private final Vibrator mVibrator;
    private final long mVibrateMillis;

    public Vibrate(Context context, long vibrateMillis) {
        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        mVibrateMillis = vibrateMillis;
    }

    public void vibrate() {
        if(mVibrateMillis > 0) {
            mVibrator.vibrate(mVibrateMillis);
        }
    }
}
