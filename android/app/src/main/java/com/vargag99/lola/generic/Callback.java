package com.vargag99.lola.generic;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Async result callback.
 *
 * @param <T>
 */
public interface Callback<T> {

    /**
     * Callback result.
     *
     * @param result Result.
     */
    void onResult(T result);

    /**
     * Callback error.
     *
     * @param e Exception.
     */
    void onError(@NonNull Exception e);

    /**
     * Runnable that delivers a Callback result.
     *
     * @param <T>
     */
    class Result<T> implements Runnable {
        @NonNull
        private final Callback<T> mCallback;
        @Nullable
        private final T mResult;

        public Result(@NonNull Callback<T> callback, @Nullable T result) {
            mCallback = callback;
            mResult = result;
        }

        @Override
        public void run() {
            mCallback.onResult(mResult);
        }
    }

    /**
     * Runnable that delivers a Callback error.
     */
    class Error implements Runnable {
        @NonNull
        private final Callback<?> mCallback;
        @NonNull
        private final Exception mException;

        public Error(@NonNull Callback<?> callback, @NonNull Exception exception) {
            mCallback = callback;
            mException = exception;
        }

        @Override
        public void run() {
            mCallback.onError(mException);
        }
    }
}
