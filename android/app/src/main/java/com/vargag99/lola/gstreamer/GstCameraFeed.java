package com.vargag99.lola.gstreamer;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Surface;
import android.view.SurfaceHolder;

import com.google.common.net.HostAndPort;
import com.vargag99.lola.generic.Callback;
import com.vargag99.lola.generic.Util;
import com.vargag99.lola.session.CameraConfig;
import com.vargag99.lola.session.Session;

/**
 * Camera feed.
 * The feed binds the Gstreamer player with its surface and the user camera setting (on or off).
 * The binding is very simple, all necessary components must be present to start the camera feed:
 * - Connected session to start the camera source.
 * - Valid CameraConfig;
 * - Usable surface to render to;
 * - The feed must be enabled (user setting).
 * When everything is ready, both the camera and the playback is started (in correct order). Each
 * playback uses a new player instance.
 */
public class GstCameraFeed {
    @NonNull
    private final Session mSession; // Signaling channel to start and stop the camera.
    private boolean mEnabled; // User setting (from the Toolbar button).
    @Nullable
    private CameraConfig mCameraConfig; // Camera config to be used.
    @Nullable
    private HostAndPort mClientHostAndPort; // Hack - we should get this back from the player.
    @Nullable
    private Player mPlayer; // Player.
    @Nullable
    private SurfaceHolder mSurfaceHolder; // Surface holder.
    @Nullable
    private Surface mSurface; // Surface.
    @NonNull
    private State mState = State.OFF; // State.
    @Nullable
    private Listener mListener; // Listener.

    // Listeners and callbacks. These are all part of this GstCameraFeed object.
    private final SurfaceCallback mSurfaceCallback = new SurfaceCallback();
    private final PlayerListener mPlayerListener = new PlayerListener();
    private final CameraListener mCameraStartListener = new CameraListener();

    public enum State {
        OFF, // Camera feed is off, for one of the dozen reasons.
        STARTING_PLAYER, // Intermediate state, starting player.
        STARTING_CAMERA, // Intermediate state, starting the camera.
        PLAYING, // Playing.
        ERROR // Error.
    }

    public GstCameraFeed(@NonNull Session session) {
        mSession = session;
    }

    /**
     * Camera feed listener.
     */
    public interface Listener {
        void onStateChanged(@NonNull GstCameraFeed cameraFeed, @NonNull State state);
    }

    public void setListener(@Nullable Listener listener) {
        mListener = listener;
    }

    @NonNull
    public State getState() {
        return mState;
    }

    public void setCameraConfig(@Nullable CameraConfig cameraConfig) {
        mCameraConfig = cameraConfig;
        update();
    }

    public void setSurfaceHolder(@Nullable SurfaceHolder surfaceHolder) {
        // Clean up old surface, if any.
        if (mSurfaceHolder != null) {
            mSurfaceHolder.removeCallback(mSurfaceCallback);
        }
        mSurfaceHolder = null;
        mSurface = null;
        update();
        // Set up new surface, if any.
        mSurfaceHolder = surfaceHolder;
        if (mSurfaceHolder != null) {
            mSurfaceHolder.addCallback(mSurfaceCallback);
            mSurface = mSurfaceHolder.getSurface();
            update();
        }
    }

    public void setEnabled(boolean enable) {
        mEnabled = enable;
        update();
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    private void update() {
        if (mEnabled && mSurface != null && mCameraConfig != null) {
            // We need the feed and can do it. Start.
            if (mPlayer == null) {
                if (!mCameraConfig.connectionOriented) {
                    // Connectionless mode - start the player first.
                    // TODO We should start the player and get back its listening address.
                    // TODO for now we just use a fixed port.
                    String clientHost = Util.getLocalIpAddress();
                    if (clientHost == null) {
                        setState(State.ERROR);
                        return;
                    }
                    mClientHostAndPort = HostAndPort.fromParts(clientHost, 5004);
                    String clientConfig = mCameraConfig.clientConfig
                            .replace("{clientHost}", mClientHostAndPort.getHostText())
                            .replace("{clientPort}", Integer.toString(mClientHostAndPort.getPort()));
                    openPlayer(clientConfig);
                    setState(State.STARTING_PLAYER);
                } else {
                    // Connection-oriented mode - start the camera first.
                    if (mState.equals(State.STARTING_CAMERA)) {
                        return; // Camera already started, waiting.
                    }
                    mSession.startCamera(mCameraConfig.id, null, mCameraStartListener);
                    setState(State.STARTING_CAMERA);
                }
            } else {
                // Just update the surface.
                mPlayer.setSurface(mSurface);
            }
        } else {
            // Feed not needed, or can't do it. Stop the camera and close the player.
            mSession.stopCamera();
            closePlayer();
            setState(State.OFF);
        }
    }

    private void openPlayer(String pipelineConfig) {
        if (mPlayer != null) {
            throw new IllegalStateException("Player already open");
        }
        mPlayer = new Player();
        mPlayer.setStateListener(mPlayerListener);
        mPlayer.setErrorListener(mPlayerListener);
        mPlayer.open(pipelineConfig);
        mPlayer.setSurface(mSurface);
        mPlayer.play();
    }

    private void closePlayer() {
        if (mPlayer != null) {
            mPlayer.setErrorListener(null);
            mPlayer.setStateListener(null);
            mPlayer.close();
            mPlayer = null;
        }
    }

    private void setState(@NonNull State state) {
        if (mState != state) {
            mState = state;
            if (mListener != null) {
                mListener.onStateChanged(this, mState);
            }
        }
    }

    private class CameraListener implements Callback<HostAndPort> {
        @Override
        public void onResult(HostAndPort result) {
            if (mEnabled && mSurface != null && mCameraConfig != null) {
                if (mCameraConfig.connectionOriented) {
                    // Connection-oriented mode. The camera is started, we can start the player.
                    // The player will be PLAYING when the data arrives.
                    String clientConfig = mCameraConfig.clientConfig
                            .replace("{serverHost}", result.getHostText())
                            .replace("{serverPort}", Integer.toString(result.getPort()));
                    openPlayer(clientConfig);
                    setState(State.STARTING_PLAYER);
                }
            }
        }

        @Override
        public void onError(@NonNull Exception e) {
            closePlayer();
            setState(State.ERROR);
        }
    }

    private class PlayerListener implements Player.ErrorListener, Player.StateListener {
        @Override
        public void onError(Player player, String message) {
            mSession.stopCamera();
            closePlayer();
            setState(State.ERROR);
        }

        @Override
        public void onStateChanged(Player player, Player.State state) {
            switch (state) {
                case PAUSED:
                    if (mEnabled && mSurface != null && mCameraConfig != null) {
                        if (!mCameraConfig.connectionOriented) {
                            // Connectionless mode, player started. Now start the camera too.
                            // The player will be PLAYING when the data arrives.
                            mSession.startCamera(mCameraConfig.id, mClientHostAndPort, mCameraStartListener);
                            setState(State.STARTING_CAMERA);
                        }
                    }
                    break;
                case PLAYING:
                    // Yay!
                    setState(State.PLAYING);
                    break;
            }
        }

    }

    private class SurfaceCallback implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            mSurface = mSurfaceHolder.getSurface();
            update();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            mSurface = null;
            update();
        }

    }

}
