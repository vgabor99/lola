package com.vargag99.lola.main;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

import com.vargag99.lola.R;
import com.vargag99.lola.gstreamer.GstCameraFeed;
import com.vargag99.lola.session.Session;

/**
 * Pick status icons.
 */
public class Icons {

    public Icons() {
    }

    public
    @DrawableRes
    int getSessionStatusIcon(@NonNull Session session) {
        switch (session.getStatus()) {
            case NULL:
                return R.drawable.ic_lan_white_24dp;
            case CONNECTING:
                return R.drawable.ic_lan_pending_white_24dp;
            case CONNECTED:
                return R.drawable.ic_lan_connect_white_24dp;
            case ERROR:
                return R.drawable.ic_lan_disconnect_red_24dp;
        }
        return R.drawable.ic_lan_white_24dp;
    }

    public
    @DrawableRes
    int getCameraFeedIcon(@NonNull GstCameraFeed cameraFeed) {
        switch (cameraFeed.getState()) {
            case OFF:
                return R.drawable.ic_videocam_off_white_24dp;
            case STARTING_PLAYER:
                return R.drawable.ic_videocam_starting_white_24dp;
            case STARTING_CAMERA:
                return R.drawable.ic_videocam_starting_white_24dp;
            case PLAYING:
                return R.drawable.ic_videocam_white_24dp;
            case ERROR:
                return R.drawable.ic_videocam_cross_red_24dp;
        }
        return R.drawable.ic_videocam_white_24dp;
    }

}
