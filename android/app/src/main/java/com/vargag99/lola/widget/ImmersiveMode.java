package com.vargag99.lola.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.vargag99.lola.R;

import java.util.ArrayList;
import java.util.List;

/**
 * This class handles immersive mode with system bars.
 * Move our top-left and top-right containers along with the system bars.
 */
public class ImmersiveMode {
    @NonNull
    private final View mWindowDecorView;
    @Nullable
    private final View mTopLeft;
    @Nullable
    private final View mTopRight;
    private final int mStatusBarHeight;
    private final int mNavigationBarWidth;
    @NonNull
    private final Handler mHandler;
    private AnimatorSet mAnimator;

    public ImmersiveMode(@NonNull View windowDecorView, @Nullable View topLeft, @Nullable View topRight) {
        mWindowDecorView = windowDecorView;
        mTopLeft = topLeft;
        mTopRight = topRight;
        mStatusBarHeight = topRight.getResources().getDimensionPixelSize(R.dimen.status_bar_height);
        mNavigationBarWidth = topRight.getResources().getDimensionPixelSize(R.dimen.navigation_bar_width);
        mHandler = new Handler();
        onCreate();
    }

    private void onCreate() {
        mWindowDecorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // This listener does two things:
                        // - Toolbar hide/show follows the system bar hide/show;
                        // - Uses a timer to do our own "sticky" immersive mode.
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            // The system bars are visible. Show the toolbar too.
                            followShow();
                            // This is the "sticky immersive" behavior. We have to do stickyness
                            // manually because if we use the SYSTEM_UI_FLAG_IMMERSIVE_STICKY flag,
                            // we never get this callback.
                            delayedHideSystemBars();
                        } else {
                            // The system bars are NOT visible.
                            followHide();
                        }
                    }
                });
    }

    public void showSystemBars() {
        // Leave immersive mode (show system bars).
        // Our containers will follow via the onSystemUiVisibilityChange() callback.
        cancel();
        mWindowDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public void hideSystemBars() {
        // Go immersive (hide system bars).
        // Our containers will follow via the onSystemUiVisibilityChange() callback.
        cancel();
        mWindowDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    public void delayedHideSystemBars() {
        mHandler.postDelayed(mDelayedHideSystemBars, 2000);
    }

    private void followHide() {
        if (mAnimator != null) {
            mAnimator.cancel();
            mAnimator = null;
        }
        List<Animator> animators = new ArrayList<>();
        if (mTopRight != null) {
            animators.add(ObjectAnimator.ofFloat(mTopRight, "translationX", 0.f));
            animators.add(ObjectAnimator.ofFloat(mTopRight, "translationY", 0));
        }
        if (mTopLeft != null) {
            animators.add(ObjectAnimator.ofFloat(mTopLeft, "translationY", 0));
        }
        mAnimator = new AnimatorSet();
        mAnimator.setDuration(mWindowDecorView.getResources().getInteger(android.R.integer.config_mediumAnimTime));
        mAnimator.setInterpolator(new AccelerateInterpolator());
        mAnimator.playTogether(animators);
        mAnimator.start();
    }

    private void followShow() {
        if (mAnimator != null) {
            mAnimator.cancel();
            mAnimator = null;
        }
        List<Animator> animators = new ArrayList<>();
        if (mTopRight != null) {
            animators.add(ObjectAnimator.ofFloat(mTopRight, "translationX", -mNavigationBarWidth));
            animators.add(ObjectAnimator.ofFloat(mTopRight, "translationY", mStatusBarHeight));
        }
        if (mTopLeft != null) {
            animators.add(ObjectAnimator.ofFloat(mTopLeft, "translationY", mStatusBarHeight));
        }
        mAnimator = new AnimatorSet();
        mAnimator.setDuration(mWindowDecorView.getResources().getInteger(android.R.integer.config_shortAnimTime));
        mAnimator.setInterpolator(new DecelerateInterpolator());
        mAnimator.playTogether(animators);
        mAnimator.start();
    }

    private void cancel() {
        mHandler.removeCallbacks(mDelayedHideSystemBars);
    }

    private final Runnable mDelayedHideSystemBars = new Runnable() {
        @Override
        public void run() {
            hideSystemBars();
        }
    };

}
