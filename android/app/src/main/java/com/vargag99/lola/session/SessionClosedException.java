package com.vargag99.lola.session;

import java.io.IOException;

/**
 * Session closed exception.
 */
public class SessionClosedException extends IOException {
    public SessionClosedException() {
        super("Session closed");
    }
}
