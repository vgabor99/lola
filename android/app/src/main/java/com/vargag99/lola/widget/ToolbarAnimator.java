package com.vargag99.lola.widget;

import android.support.annotation.NonNull;
import android.view.View;

/**
 * Show / hide the Toolbar with animation (slide in, slide out).
 */
public class ToolbarAnimator {
    @NonNull
    private final View mToolbar;

    public ToolbarAnimator(@NonNull View toolbar) {
        mToolbar = toolbar;
    }

    public void hide() {
        cancel();
        int height = mToolbar.getHeight();
        mToolbar.animate()
                .translationY(-height)
                .setDuration(mToolbar.getResources().getInteger(android.R.integer.config_shortAnimTime))
                .start();
    }

    public void show() {
        cancel();
        mToolbar.animate()
                .translationY(0)
                .setDuration(mToolbar.getResources().getInteger(android.R.integer.config_shortAnimTime))
                .start();
    }

    public void cancel() {
        mToolbar.clearAnimation();
    }
}
