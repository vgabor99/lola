package com.vargag99.lola.generic;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;

import com.google.gson.Gson;
import com.vargag99.lola.session.CameraConfig;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * Bits and pieces.
 */
public class Util {

    @Nullable
    public static CameraConfig findConfig(@NonNull List<CameraConfig> cameraConfigs, String id) {
        for (CameraConfig config : cameraConfigs) {
            if (config.id.equals(id)) {
                return config;
            }
        }
        return null;
    }

    @NonNull
    public static List<CameraConfig> fromJson(@NonNull String json) {
        return Arrays.asList(new Gson().fromJson(json, CameraConfig[].class));
    }

    @NonNull
    public static String toJson(List<CameraConfig> cameraConfigs) {
        return new Gson().toJson(cameraConfigs);
    }

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ignored) {
        }
        return null;
    }

    public static String emptyIfNull(String s) {
        return (s == null) ? "" : s;
    }

    public static float dpToPixel(Context context, int dp) {
        return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    private Util() {
        // Static methods only.
    }
}
