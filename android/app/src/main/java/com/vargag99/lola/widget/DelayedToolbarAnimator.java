package com.vargag99.lola.widget;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;

/**
 * Add "delayed hide" behavior to ToolbarAnimator.
 */
public class DelayedToolbarAnimator extends ToolbarAnimator {
    private final Handler mHandler;

    public DelayedToolbarAnimator(@NonNull View toolbar) {
        super(toolbar);
        mHandler = new Handler();
    }

    @Override
    public void cancel() {
        super.cancel();
        mHandler.removeCallbacks(mDelayedHide);
    }

    public void delayedHide() {
        mHandler.postDelayed(mDelayedHide, 2000);
    }

    private final Runnable mDelayedHide = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
}
