package com.vargag99.lola.settings;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.XpPreferenceFragment;
import android.support.v7.widget.PreferenceDividerDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.common.net.HostAndPort;
import com.vargag99.lola.R;
import com.vargag99.lola.generic.BackHandler;
import com.vargag99.lola.session.CameraConfig;

import net.xpece.android.support.preference.ListPreference;
import net.xpece.android.support.preference.PreferenceScreenNavigationStrategy;

import java.util.List;

/**
 * Settings fragment.
 */
public class SettingsFragment extends XpPreferenceFragment implements BackHandler,
        PreferenceScreenNavigationStrategy.Callbacks {
    private static final String TAG = SettingsFragment.class.getSimpleName();

    private PreferenceScreenNavigationStrategy mPreferenceScreenNavigation;

    private static final Preference.OnPreferenceChangeListener mPreferenceSummaryUpdater = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = PreferenceSummary.getSummary(preference, value);
            preference.setSummary(stringValue);
            return true;
        }
    };

    public static SettingsFragment newInstance(String rootKey) {
        Bundle args = new Bundle();
        args.putString(SettingsFragment.ARG_PREFERENCE_ROOT, rootKey);
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreatePreferences2(final Bundle savedInstanceState, final String rootKey) {
        // Add 'general' preferences.
        addPreferencesFromResource(R.xml.preferences);
        populateCameraConfig();
        bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_address)));
        bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_camera_config)));
        findPreference(getString(R.string.pref_key_about)).setSummary(getAboutText(getActivity()));
        findPreference(getString(R.string.pref_key_home_page)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                launchHomePage();
                return true;
            }
        });
        // Setup root preference key from arguments.
        getPreferenceScreen().setKey(rootKey);
        mPreferenceScreenNavigation = new PreferenceScreenNavigationStrategy.ReplaceRoot(this, this);
        mPreferenceScreenNavigation.onCreate(savedInstanceState);
    }

    private void populateCameraConfig() {
        ListPreference pref = (ListPreference) findPreference(getString(R.string.pref_key_camera_config));
        if (pref != null) {
            List<CameraConfig> cameraConfigs = new Preferences(getContext()).getCameraConfigs();
            CharSequence entries[] = new String[cameraConfigs.size()];
            CharSequence entryValues[] = new String[cameraConfigs.size()];
            int i = 0;
            for (CameraConfig cameraConfig : cameraConfigs) {
                entries[i] = cameraConfig.description;
                entryValues[i] = cameraConfig.id;
                i++;
            }
            pref.setEntries(entries);
            pref.setEntryValues(entryValues);
        }
    }

    private static String getAboutText(Context context) {
        PackageManager packageManager = context.getPackageManager();
        StringBuilder builder = new StringBuilder();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
            String appName = packageManager.getApplicationLabel(applicationInfo).toString();
            builder.append(appName);
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = packageInfo.versionName;
            builder.append(' ');
            builder.append(version);
            builder.append('\n');
            builder.append(context.getString(R.string.msg_copyright));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private void launchHomePage() {
        // The pi hosts the site, so we simply use its address.
        Preferences preferences = new Preferences(getActivity());
        HostAndPort address = preferences.getAddress();
        Uri uri = Uri.fromParts("http", address.getHostText(), null);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "launchHomePage", e);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mPreferenceScreenNavigation.onSaveInstanceState(outState);
    }

    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(mPreferenceSummaryUpdater);
        // Trigger the listener immediately with the preference's current value.
        String currentValue = PreferenceSummary.getCurrentValueAsString(preference);
        mPreferenceSummaryUpdater.onPreferenceChange(preference, currentValue);
    }

    @Override
    public void onRecyclerViewCreated(RecyclerView list) {
        list.addItemDecoration(new PreferenceDividerDecoration(getContext()).drawBottom(true));
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference instanceof PreferenceScreen) {
            PreferenceScreen preferenceScreen = (PreferenceScreen) preference;
            mPreferenceScreenNavigation.onPreferenceScreenClick(preferenceScreen);
            return true;
        }
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public boolean onBackPressed() {
        if (mPreferenceScreenNavigation.onBackPressed()) {
            return true;
        }
        return false;
    }

    @Override
    public void onNavigateToPreferenceScreen(PreferenceScreen preferenceScreen) {
        getActivity().setTitle(preferenceScreen.getTitle());
    }
}
