package com.vargag99.lola.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;
import com.vargag99.lola.R;

/**
 * Gauge view.
 */
public class GaugeView extends View {
    private final static String TAG = GaugeView.class.getSimpleName();

    private float mScaleWidth;
    private int mScaleColors[];
    private Paint mScalePaint = new Paint();
    private Paint mGapPaint = new Paint();
    private float mHandHalfWidth;
    private float mHandTipHalfWidth;
    private int mHandColor;
    private Paint mHandPaint = new Paint();
    private Path mHandPath = new Path();
    private Path mGapPath = new Path();
    private int mStartAngle;
    private int mSweepAngle;
    private int mStartPaddingAngle;
    private int mEndPaddingAngle;
    private float mScaleGap;
    private int[] mValues;
    private int mMinValue;
    private int mMaxValue;

    private RectF mScaleRect = new RectF();
    private PointF mScaleCenter = new PointF();
    private float mScaleRadius;
    private float[] mScaleAngles;

    private final SpringSystem mSpringSystem;
    private final Spring mSpring;

    public GaugeView(Context context) {
        this(context, null);
    }

    public GaugeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GaugeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.GaugeView, defStyleAttr, 0);
        mScaleWidth = a.getDimension(R.styleable.GaugeView_gaugeScaleWidth, 10);
        int id = a.getResourceId(R.styleable.GaugeView_gaugeScaleColors, 0);
        if (id != 0 && !isInEditMode()) {
            // Colors array is set.
            if (a.hasValue(R.styleable.GaugeView_gaugeScaleColor)) {
                throw new IllegalArgumentException("Cannot set gaugeScaleColors array and gaugeScaleColor at the same time");
            }
            TypedArray ta = context.getResources().obtainTypedArray(id);
            mScaleColors = new int[ta.length()];
            for (int i = 0; i < ta.length(); i++) {
                mScaleColors[i] = ta.getColor(i, 0);
            }
            ta.recycle();
        } else {
            // color is set.
            int scaleColor = a.getColor(R.styleable.GaugeView_gaugeScaleColor, ContextCompat.getColor(context, android.R.color.darker_gray));
            mScaleColors = new int[]{scaleColor};
        }
        mHandHalfWidth = a.getDimension(R.styleable.GaugeView_gaugeHandWidth, 10) / 2.f;
        mHandTipHalfWidth = a.getDimension(R.styleable.GaugeView_gaugeHandTipWidth, mHandHalfWidth / 4) / 2.f;
        mHandColor = a.getColor(R.styleable.GaugeView_gaugeHandColor, ContextCompat.getColor(context, android.R.color.holo_red_dark));
        mStartAngle = a.getInt(R.styleable.GaugeView_gaugeStartAngle, 0);
        mSweepAngle = a.getInt(R.styleable.GaugeView_gaugeSweepAngle, 360);
        mStartPaddingAngle = a.getInt(R.styleable.GaugeView_gaugeStartPaddingAngle, 0);
        mEndPaddingAngle = a.getInt(R.styleable.GaugeView_gaugeEndPaddingAngle, 0);
        id = a.getResourceId(R.styleable.GaugeView_gaugeValues, 0);
        if (id != 0) {
            // Values array is set.
            if (a.hasValue(R.styleable.GaugeView_gaugeMinValue) || a.hasValue(R.styleable.GaugeView_gaugeMaxValue)) {
                throw new IllegalArgumentException("Cannot set gaugeValues array and gaugeMinValue/gaugeMaxValue at the same time");
            }
            mValues = context.getResources().getIntArray(id);
            if (mValues.length < 2) {
                throw new IllegalArgumentException("At least two values are required in gaugeValues");
            }
            mMinValue = mValues[0];
            mMaxValue = mValues[mValues.length - 1];
        } else {
            // minValue and maxValue are set.
            mMinValue = a.getInt(R.styleable.GaugeView_gaugeMinValue, 0);
            mMaxValue = a.getInt(R.styleable.GaugeView_gaugeMaxValue, 100);
            mValues = new int[]{mMinValue, mMaxValue};
        }

        mScaleGap = a.getDimension(R.styleable.GaugeView_gaugeScaleGap, 0);
        int curValue = a.getInt(R.styleable.GaugeView_gaugeCurValue, mMinValue);

        int tension = a.getInt(R.styleable.GaugeView_gaugeSpringTension, 40);
        int friction = a.getInt(R.styleable.GaugeView_gaugeSpringFriction, 7);
        boolean overshootClamping = a.getBoolean(R.styleable.GaugeView_gaugeSpringOvershootClamping, false);

        a.recycle();

        mScalePaint.setStrokeWidth(mScaleWidth);
        mScalePaint.setAntiAlias(true);
        mScalePaint.setStrokeCap(Paint.Cap.BUTT);
        mScalePaint.setStyle(Paint.Style.STROKE);

        mGapPaint.setColor(Color.TRANSPARENT);
        mGapPaint.setAlpha(0);
        mGapPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        mGapPaint.setStrokeWidth(mScaleGap);
        mGapPaint.setStrokeCap(Paint.Cap.BUTT);
        mGapPaint.setAntiAlias(true);
        mGapPaint.setStyle(Paint.Style.STROKE);
        if (mScaleGap > 0) {
            // TODO erasing canvas not working on HW accelerated layer (gets black instead).
            // TODO fix this - this gets slow.
            setLayerType(LAYER_TYPE_SOFTWARE, null);
        }

        mHandPaint.setColor(mHandColor);
        mHandPaint.setStrokeJoin(Paint.Join.MITER);
        mHandPaint.setAntiAlias(true);
        mHandPaint.setStyle(Paint.Style.FILL);

        mSpringSystem = SpringSystem.create();
        mSpring = mSpringSystem.createSpring();
        mSpring.setSpringConfig(SpringConfig.fromOrigamiTensionAndFriction(tension, friction));
        mSpring.setOvershootClampingEnabled(overshootClamping);
        mSpring.setCurrentValue(curValue, true);
        mSpring.addListener(new SimpleSpringListener() {
            @Override
            public void onSpringUpdate(Spring spring) {
                setNeedle((int) spring.getCurrentValue());
            }
        });
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Rect rect = new Rect();
        getLocalVisibleRect(rect);
        int width = getWidth() - getPaddingLeft() - getPaddingRight();
        int height = getHeight() - getPaddingTop() - getPaddingBottom();
        mScaleRadius = (Math.min(width, height) - mScaleWidth) / 2.f;
        mScaleCenter.set(rect.exactCenterX(), rect.exactCenterY());
        mScaleRect.set(mScaleCenter.x - mScaleRadius, mScaleCenter.y - mScaleRadius,
                mScaleCenter.x + mScaleRadius, mScaleCenter.y + mScaleRadius);
        // Compute scale angles.
        // The first and last are special (they match the start/end excatly, so the scale start/end
        // offsets are included in them). The middle ones are computed.
        mScaleAngles = new float[mValues.length];
        mScaleAngles[0] = mStartAngle;
        for (int i = 1; i < mValues.length - 1; i++) {
            mScaleAngles[i] = angleOf(mValues[i]);
        }
        mScaleAngles[mValues.length - 1] = mStartAngle + mSweepAngle;
        // Draw gap path.
        mGapPath.reset();
        mGapPath.moveTo(mScaleCenter.x, mScaleCenter.y);
        mGapPath.lineTo(width, mScaleCenter.y);
        // Draw hand path.
        mHandPath.reset();
        mHandPath.moveTo(mScaleCenter.x + mScaleRadius, mScaleCenter.y + mHandTipHalfWidth);
        mHandPath.arcTo(new RectF(mScaleCenter.x - mHandHalfWidth, mScaleCenter.y - mHandHalfWidth, mScaleCenter.x + mHandHalfWidth, mScaleCenter.y + mHandHalfWidth), 90, 180.f, false);
        mHandPath.lineTo(mScaleCenter.x + mScaleRadius, mScaleCenter.y - mHandTipHalfWidth);
        mHandPath.close();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // Draw scale.
        for (int i = 0; i < mScaleAngles.length - 1; i++) {
            mScalePaint.setColor(mScaleColors[Math.min(i, mScaleColors.length - 1)]);
            canvas.drawArc(mScaleRect, mScaleAngles[i], mScaleAngles[i + 1] - mScaleAngles[i], false, mScalePaint);
        }
        // Clear the gaps.
        if (mScaleGap > 0) {
            for (int i = 1; i < mScaleAngles.length - 1; i++) {
                canvas.save(Canvas.MATRIX_SAVE_FLAG);
                canvas.rotate(mScaleAngles[i], mScaleCenter.x, mScaleCenter.y);
                canvas.drawPath(mGapPath, mGapPaint);
                canvas.restore();
            }
        }
        // Draw hand.
        float angle = angleOf((int) mSpring.getCurrentValue());
        canvas.save(Canvas.MATRIX_SAVE_FLAG);
        canvas.rotate(angle, mScaleCenter.x, mScaleCenter.y);
        canvas.drawPath(mHandPath, mHandPaint);
        canvas.restore();
    }

    private float angleOf(float value) {
        return mStartAngle + mStartPaddingAngle + sweepOf(value - mMinValue);
    }

    private float sweepOf(float delta) {
        return (mSweepAngle - mStartPaddingAngle - mEndPaddingAngle) * delta / (mMaxValue - mMinValue);
    }

    private int clampValue(int value) {
        return Math.min(Math.max(value, mMinValue), mMaxValue);
    }

    public void setValue(int value) {
        mSpring.setEndValue(clampValue(value));
    }

    public int getValue() {
        return (int) mSpring.getEndValue();
    }

    private void setNeedle(int value) {
        invalidate();
    }
}
