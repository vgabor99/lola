package com.vargag99.lola.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.vargag99.lola.R;

/**
 * Fixed aspect ratio frame layout.
 */
public class FixedAspectRatioFrameLayout extends FrameLayout {
    private float mAspectRatio;

    public FixedAspectRatioFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FixedAspectRatioFrameLayout);
        mAspectRatio = a.getFloat(R.styleable.FixedAspectRatioFrameLayout_aspectRatio, 1.3333f);
        a.recycle();
    }

    public float getAspectRatio() {
        return mAspectRatio;
    }

    public void setAspectRatio(float aspectRatio) {
        mAspectRatio = aspectRatio;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int receivedWidth = MeasureSpec.getSize(widthMeasureSpec);
        int receivedHeight = MeasureSpec.getSize(heightMeasureSpec);

        int measuredWidth;
        int measuredHeight;
        boolean widthDynamic = widthMode == MeasureSpec.EXACTLY && receivedWidth == 0;
        boolean heightDynamic = heightMode == MeasureSpec.EXACTLY && receivedHeight == 0;
        if (widthDynamic && heightDynamic) {
            throw new IllegalArgumentException("Both width and height is dynamic");
        } else if (widthDynamic) {
            // Width is dynamic.
            int w = (int) (receivedHeight * mAspectRatio);
            measuredWidth = MeasureSpec.makeMeasureSpec(w, MeasureSpec.EXACTLY);
            measuredHeight = heightMeasureSpec;
            super.onMeasure(measuredWidth, measuredHeight);
        } else if (heightDynamic) {
            // Height is dynamic.
            measuredWidth = widthMeasureSpec;
            int h = (int) (receivedWidth / mAspectRatio);
            measuredHeight = MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY);
            super.onMeasure(measuredWidth, measuredHeight);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
