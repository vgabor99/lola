package com.vargag99.lola.session;

import android.support.annotation.NonNull;

/**
 * Transport RPC commands.
 */
public interface ClientTransport {
    /**
     * Connect the transport channel.
     *
     * @throws Exception
     */
    void connect() throws Exception;

    /**
     * Close the transport channel.
     */
    void close();

    /**
     * Call an RPC method.
     *
     * @param request Request.
     * @return Response.
     * @throws Exception
     */
    @NonNull
    String call(@NonNull String request) throws Exception;

    /**
     * Send a request.
     *
     * @param request Request.
     * @throws Exception
     */
    void send(@NonNull String request) throws Exception;

    /**
     * Receive response.
     *
     * @return Response.
     * @throws Exception
     */
    @NonNull
    String receive() throws Exception;
}
