package com.vargag99.lola.gstreamer;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.Surface;

import org.freedesktop.gstreamer.GStreamer;

import java.io.Closeable;

/**
 * Java interface to GStreamer.
 */
public class Player implements Closeable {
    private final static String TAG = Player.class.getSimpleName();
    private final Handler mHandler;
    private static boolean mInitDone;

    private native void nativeInit(String pipelineConfig);     // Initialize native code, build pipeline, etc

    private native void nativeFinalize(); // Destroy pipeline and shutdown native code

    private native void nativePlay();     // Set pipeline to PLAYING

    private native void nativePause();    // Set pipeline to PAUSED

    private static native void nativeClassInit(); // Initialize native class: cache Method IDs for callbacks

    private native void nativeSurfaceInit(Object surface);

    private native void nativeSurfaceFinalize();

    private static native void nativeSetDebugThreshold(String list, boolean reset);

    private long mNativeCustomData; // Native code will use this to keep private data

    private boolean mPlayRequested; // Whether the user asked to go to PLAYING

    public static void init(Context context) throws Exception {
        if(!mInitDone) {
            System.loadLibrary("gstreamer_android");
            GStreamer.init(context);
            System.loadLibrary("gstplayer");
            setDebugThreshold("gstplayer:5", true);
            nativeClassInit();
            mInitDone = true;
        }
    }

    public static boolean isInitDone() {
        return mInitDone;
    }

    public static void setDebugThreshold(String list, boolean reset) {
        nativeSetDebugThreshold(list, reset);
    }

    public Player() {
        mHandler = new Handler();
    }

    public void open(String pipelineConfig) {
        nativeInit(pipelineConfig);
    }

    @Override
    public void close() {
        nativeFinalize();
    }

    public void play() {
        mPlayRequested = true;
        nativePlay();
    }

    public void pause() {
        mPlayRequested = false;
        nativePause();
    }

    public boolean isPlaying() {
        return mPlayRequested;
    }

    public void setSurface(Surface surface) {
        if (surface == null) {
            nativeSurfaceFinalize();
        } else {
            nativeSurfaceInit(surface);
        }
    }

    /**
     * Error listener.
     */
    public interface ErrorListener {
        void onError(Player player, String message);
    }

    private ErrorListener mErrorListener;

    public void setErrorListener(ErrorListener listener) {
        mErrorListener = listener;
    }

    // Called from native code.
    private void onError(final String message) {
        Log.i(TAG, "onError:" + message);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mErrorListener != null) {
                    mErrorListener.onError(Player.this, message);
                }
            }
        });
    }

    public enum State {
        VOID_PENDING,
        NULL,
        READY,
        PAUSED,
        PLAYING
    }

    /**
     * State change listener.
     */
    public interface StateListener {
        void onStateChanged(Player player, State state);
    }

    private StateListener mStateListener;

    public void setStateListener(StateListener listener) {
        mStateListener = listener;
    }

    // Called from native code.
    private void onStateChanged(final int state) {
        Log.i(TAG, "onStateChanged:" + state);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mStateListener != null) {
                    State s = State.VOID_PENDING;
                    switch (state) {
                        case 0:
                            s = State.VOID_PENDING;
                            break;
                        case 1:
                            s = State.NULL;
                            break;
                        case 2:
                            s = State.READY;
                            break;
                        case 3:
                            s = State.PAUSED;
                            break;
                        case 4:
                            s = State.PLAYING;
                            break;
                    }
                    mStateListener.onStateChanged(Player.this, s);
                }
            }
        });
    }

    // Called from native code. Native code calls this once it has created its pipeline and
    // the main loop is running, so it is ready to accept commands.
    private void onGStreamerInitialized() {
        Log.i(TAG, "onGStreamerInitialized, mPlayRequested:" + mPlayRequested);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                // Restore previous playing state
                if (mPlayRequested) {
                    nativePlay();
                } else {
                    nativePause();
                }
            }
        });
    }

}
