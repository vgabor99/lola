#!/usr/bin/env python
# coding: utf-8

from camera import Camera
from driver import Driver
from remote import Remote

# Make the driver and the camera
driver = Driver()
camera = Camera()

# The remote is used to control the driver and the camera.
remote = Remote(driver, camera)

# The method map contains the remotable methods.
rpc_methods = {'connect': remote.connect,
              'close': remote.close,
              'driveMotorSteer': remote.driveMotorSteer,
              'driveSpeedAngle': remote.driveSpeedAngle,
              'driveTracks': remote.driveTracks,
              'headlights': remote.headlights,
              'ping': remote.ping,
              'getCameraConfigurations': remote.getCameraConfigurations,
              'startCamera': remote.startCamera,
              'stopCamera': remote.stopCamera,
              }
