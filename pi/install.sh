#!/bin/sh

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
SRC_DIR=$SCRIPTPATH
INSTALL_DIR=/usr/local/bin/piremote
DAEMON=$INSTALL_DIR/piremote.py

cp -r $SRC_DIR/* $INSTALL_DIR
dos2unix $DAEMON
chmod 755 $DAEMON
