#!/usr/bin/env python
# coding: utf-8

import time
from threading import Timer
from functools import wraps

class InactivityMonitor():
    '''Inactivity monitor. Unless regularly reset, inactivity is detected.'''

    def __init__(self, timeout, func):
        self.timeout = timeout
        self.func = func
        self.restart()

    def restart(self):
        '''Restart the inactivity monitor'''
        self.expires = time.time() + self.timeout
        t = Timer(self.timeout + 0.1, self.check) #Add safety margin
        t.start()

    def check(self):
        if(time.time() > self.expires):
            self.func()

def reset(inactivityMonitor):
    '''Decorator that resets the inactivity monitor after the decorated call'''
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            inactivityMonitor.restart()
            return result
        return wrapper
    return decorator
