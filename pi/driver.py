#!/usr/bin/env python
# coding: utf-8

from __future__ import division

import gpio


def pwmDutyCycle(value):
    ''' Scale logical driving levels to duty cycle.
    Minimum duty cycle: the motors need a minimum voltage to start.
    Maximum duty cycle: we can limit maximum power to motors if they are too fast or draw too much current.
    We map the 0...100 range to minDutyCycle...maxDutyCycle'''
    minDutyCycle = 15  # PWM level where the motor is about to start.
    maxDutyCycle = 100 # PWM level where we want to stop, valid values minDutyCycle ... 100
    if (value == 0):
        return 0
    return minDutyCycle + (maxDutyCycle - minDutyCycle) * value / 100


class Driver:
    '''Drive the vehicle.

    Control gpio pins for driving.
    '''

    def stop(self):
        '''Stop the motor.'''
        gpio.rightPwm.stop()
        gpio.output(gpio.pinRightForward, gpio.LOW)
        gpio.output(gpio.pinRightBackward, gpio.LOW)
        gpio.leftPwm.stop()
        gpio.output(gpio.pinLeftForward, gpio.LOW)
        gpio.output(gpio.pinLeftBackward, gpio.LOW)
        print 'driver stop'

    def driveMotorSteer(self, motor, steer):
        '''Drive by motor and the wheel values (rectangular joystick)'''
        if (motor > 100 or motor < -100):
            raise ValueError("Invalid motor argument {}".format(motor))
        if (steer > 100 or steer < -100):
            raise ValueError("Invalid steer argument {}".format(steer))
        left, right = self.motorSteer201x201ToTracks(motor, steer)  # Logical levels
        print 'driveMotorSteer motor={} steer={} left={} right={}'.format(str(motor), str(steer), str(left), str(right))
        self.driveTracks(left, right)

    def driveSpeedAngle(self, speed, angle):
        '''Drive by speed and angle (round directional joystick)'''
        if (speed > 100 or speed < -100):
            raise ValueError("Invalid speed argument {}".format(speed))
        if (angle > 359 or angle < 0):
            raise ValueError("Invalid angle argument {}".format(angle))
        left, right = self.speedAngleToTracks(speed, angle)  # Logical levels
        print 'driveSpeedAngle speed={} angle={} left={} right={}'.format(str(speed), str(angle), str(left), str(right))
        self.driveTracks(left, right)

    def driveTracks(self, left, right):
        '''Drive the left and right tracks'''
        if (left > 100 or left < -100):
            raise ValueError("Invalid left argument {}".format(left))
        if (right > 100 or right < -100):
            raise ValueError("Invalid right argument {}".format(right))
        print 'driveTracks left={} right={}'.format(str(left), str(right))
        gpio.output(gpio.pinLeftForward, gpio.HIGH if left > 0 else gpio.LOW)
        gpio.output(gpio.pinLeftBackward, gpio.HIGH if left < 0 else gpio.LOW)
        gpio.leftPwm.start(pwmDutyCycle(abs(left)))
        gpio.output(gpio.pinRightForward, gpio.HIGH if right > 0 else gpio.LOW)
        gpio.output(gpio.pinRightBackward, gpio.HIGH if right < 0 else gpio.LOW)
        gpio.rightPwm.start(pwmDutyCycle(abs(right)))

    def motorSteer3x3ToTracks(self, motor, steer):
        '''Map 3x3 digital drive to track speeds'''
        left, right = 0, 0
        if (motor > 0):
            if (steer > 0):
                left = 100
                right = 30
            elif (steer < 0):
                left = 30
                right = 100
            else:
                left = right = 100
        elif (motor < 0):
            if (steer > 0):
                left = -100
                right = -30
            elif (steer < 0):
                left = -30
                right = -100
            else:
                left = right = -100
        else:
            if (steer > 0):
                left = 100
                right = -100
            elif (steer < 0):
                left = -100
                right = 100
            else:
                left = right = 0
        return left, right

    def motorSteer201x201ToTracks(self, motor, steer):
        '''Map 201x201 analog drive to track speeds'''
        steer = steer * (
            1 - 0.5 * (abs(motor) / 100))  # Adaptive steering - the faster we go, the less sensitive is steering.
        left = motor + steer
        right = motor - steer
        magnitude = max(abs(left), abs(right))
        if (magnitude > 100):
            left = left * 100 / magnitude
            right = right * 100 / magnitude
        return left, right

    def speedAngleToTracks(self, speed, angle):
        '''Map speed and angle drive to track speeds'''
        left, right = 0, 0
        if (speed > 0):
            if (angle >= 270):
                # 270-360 quadrant
                left = -((360 - angle) / 90 * 200 - 100);
                right = -100;
            elif (angle >= 180):
                # 180-270 quadrant
                left = -100;
                right = (270 - angle) / 90 * 200 - 100;
            elif (angle >= 90):
                # 90-180 quadrant
                left = (180 - angle) / 90 * 200 - 100;
                right = 100;
            else:
                # 0-90 quadrant
                left = 100;
                right = -((90 - angle) / 90 * 200 - 100);
            left *= speed / 100
            right *= speed / 100
        return left, right

    def headlights(self, on):
        '''Turn the headlights on or off'''
        print 'headlights ' + str(on)
        gpio.output(gpio.pinHeadlights, gpio.HIGH if on else gpio.LOW)
