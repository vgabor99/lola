#!/usr/bin/env python
# coding: utf-8
import functools


def synchronized(lock=None):
    '''Synchronization decorator'''
    def _decorator(wrapped):
        @functools.wraps(wrapped)
        def _wrapper(*args, **kwargs):
            with lock:
                return wrapped(*args, **kwargs)
        return _wrapper
    return _decorator