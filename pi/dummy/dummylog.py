#!/usr/bin/env python
# coding: utf-8

from functools import wraps


def dummylog(func):
    '''Decorator that prints the function call'''

    @wraps(func)
    def wrapper(*args, **kwargs):
        print '{} ({})'.format(func.__name__, formatAllArgs(args, kwargs))
        return func(*args, **kwargs)

    return wrapper


def formatAllArgs(args, kwds):
    """
    makes a nice string representation of all the arguments
    """
    allargs = []
    for item in args:
        allargs.append('%s' % item)
    for key, item in kwds.items():
        allargs.append('%s=%s' % (key, item))
    formattedArgs = ', '.join(allargs)
    if len(formattedArgs) > 150:
        return formattedArgs[:146] + " ..."
    return formattedArgs
