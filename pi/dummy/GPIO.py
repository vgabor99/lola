#!/usr/bin/env python
# coding: utf-8

# Dummy GPIO to stub GPIO calls during development off the PI.

from dummylog import dummylog

BOARD = 'BOARD'
BCM = 'BCM'
OUT = 'OUT'

HIGH = 'HIGH'
LOW = 'LOW'


@dummylog
def cleanup():
    pass


@dummylog
def setmode(mode):
    pass


@dummylog
def setup(pin, value):
    pass


@dummylog
def output(pin, value):
    pass


class PWM:
    def __init__(self, pin, frequency):
        pass

    @dummylog
    def start(self, value):
        pass

    @dummylog
    def stop(self):
        pass

    @dummylog
    def ChangeDutyCycle(pwm, value):
        pass
