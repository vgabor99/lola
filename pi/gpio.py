#!/usr/bin/env python
# coding: utf-8

import atexit

try:
    from RPi import GPIO as GPIO
except ImportError:
    from dummy import GPIO as GPIO

pinRemoteConnected = 20
pinRemoteActivity = 21
pinHeadlights = 7
pinLeftSpeed = 13
pinLeftBackward = 16
pinLeftForward = 19
pinRightSpeed = 12
pinRightBackward = 5
pinRightForward = 6
pwmFrequency = 100

HIGH = GPIO.HIGH
LOW = GPIO.LOW

GPIO.setmode(GPIO.BCM)  ## Use board pin numbering
GPIO.setup(pinRemoteConnected, GPIO.OUT)
GPIO.setup(pinRemoteActivity, GPIO.OUT)
GPIO.setup(pinHeadlights, GPIO.OUT)
GPIO.setup(pinRightSpeed, GPIO.OUT)
GPIO.setup(pinRightForward, GPIO.OUT)
GPIO.setup(pinRightBackward, GPIO.OUT)
GPIO.setup(pinLeftSpeed, GPIO.OUT)
GPIO.setup(pinLeftForward, GPIO.OUT)
GPIO.setup(pinLeftBackward, GPIO.OUT)

rightPwm = GPIO.PWM(pinRightSpeed, pwmFrequency)
leftPwm = GPIO.PWM(pinLeftSpeed, pwmFrequency)

rightPwm.start(0)
leftPwm.start(0)

def cleanup():
    # Safety cleanup. Set outputs to low and cleanup GPIO.
    output(pinRemoteConnected, LOW);
    output(pinRemoteActivity, LOW);
    output(pinHeadlights, LOW);
    rightPwm.stop()
    output(pinRightForward, LOW);
    output(pinRightBackward, LOW);
    leftPwm.stop()
    output(pinLeftForward, LOW);
    output(pinLeftBackward, LOW);
    GPIO.cleanup()


def output(pin, value):
    GPIO.output(pin, value)


atexit.register(cleanup)
