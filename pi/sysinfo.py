#!/usr/bin/env python
# coding: utf-8

import subprocess
from mcp3002 import readAnalog


class SysInfo:
    '''System information'''

    def __init__(self):
        self.cpuLoad = getCpuLoad()
        self.temp = getTemp()
        self.memTotal, self.memUsed, self.memFree = getMem()
        self.batteryVoltage = getBatteryVoltage()


def getCpuLoad():
    '''Return CPU load 0-100'''
    try:
        idle = int(subprocess.check_output('vmstat').split('\n')[2].split()[14])
        return 100 - idle
    except:
        return 0


def getTemp():
    '''Get the current temperature in Celsius'''
    try:
        tmp = subprocess.check_output(['/opt/vc/bin/vcgencmd', 'measure_temp'])
        return float(tmp.split('=')[1][:-3])
    except:
        return 0


def getMem():
    '''Get total/used/free memory'''
    try:
        tmp = subprocess.check_output('free').split('\n')[1].split()
        return tmp[1], tmp[2], tmp[3]
    except:
        return 0, 0, 0


def getBatteryVoltage():
    '''Get battery voltage level'''
    try:
        # Read 0...3.3V range in 10 bit
        # The voltage divider is 100/280 kOhm
        return float(readAnalog()) / 1024 * 3.3 * 280 / 100
    except:
        return 0
