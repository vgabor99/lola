#!/usr/bin/env python
# coding: utf-8
import threading
import time
from uuid import uuid4

from tinyrpc.dispatch import public

import gpio
from complexjson import jsonpickleEncode
from inactivitymonitor import InactivityMonitor
from sync import synchronized
from sysinfo import SysInfo

lock = threading.RLock()


class ActivityIndicator:
    def __init__(self):
        self.timer = None
        gpio.output(gpio.pinRemoteActivity, gpio.LOW)

    def showActivity(self):
        if (self.timer is not None):
            self.timer.cancel()
        gpio.output(gpio.pinRemoteActivity, gpio.HIGH)
        self.timer = threading.Timer(0.05, gpio.output, [gpio.pinRemoteActivity, gpio.LOW])
        self.timer.start()


class Remote:
    ''' Remote session (i.e. remote controller) management.
    Apart from dispatching the commands to the appropriate objects, the Remote has the safety features:
    - Lag detection: reject commands that are too late.
    - Connection loss detection (a.k.a. dead man's switch): Stop the vehicle if nobody is driving.
    - Single remote control: we accept commands from the last connected session only. All other commands are rejected. '''

    def __init__(self, driver, camera):
        self.driver = driver
        self.camera = camera
        self.connectedSid = None
        self.bestWhenOffset = None
        self.activityIndicator = ActivityIndicator()
        self.deadManSwitch = InactivityMonitor(1.0, self.stop)

    def checkSid(self, sid):
        '''Check sid, it must match the connected one.
           This rejects commands from all but the last connected remote.'''
        if (sid != self.connectedSid):
            raise Exception('{} not connected'.format(sid))

    def offsetOf(self, when):
        '''Get time offset from the client originating the command and server now in millis.'''
        return time.clock() * 1000 - when

    def checkLag(self, when):
        '''Check client time offset to detect lag.
           The offset is calculated from client timestamp (when a command was originated) vs. server timestamp
           (when the command is being processed). The offset does not represent a meaningful time, because client
           and server clocks are not synchronized. However, consecutive offsets can be compared to each other,
           because client and server clocks advance at the same rate.
           We track our best command delivery time and reject anything that arrives significantly slower.
           This check is applied to commands that are better dropped than executed late (e.g. driving);
           not applied to commands that are OK to execute late (e.g. stop camera).
        '''
        whenOffset = self.offsetOf(when)
        if (whenOffset > self.bestWhenOffset + 500):
            # More than 0.5 seconds worse than our best, slowdown detected.
            raise Exception('Slowdown detected ({} millis), drop command'.format(whenOffset - self.bestWhenOffset))
        elif (whenOffset < self.bestWhenOffset):
            # This is the new best.
            self.bestWhenOffset = whenOffset

    def activity(self):
        # Reset dead man's switch
        self.deadManSwitch.restart()
        # Flash the activity LED
        self.activityIndicator.showActivity()

    @synchronized(lock)
    def stop(self):
        '''Safety stop (dead man's switch).'''
        print 'stop'
        self.driver.stop()
        # self.driver.headlights(False)
        # self.camera.stop()

    @public
    @synchronized(lock)
    def connect(self, when):
        '''Connect new session. (This drops existing session.)'''
        self.connectedSid = str(uuid4())
        gpio.output(gpio.pinRemoteConnected, gpio.HIGH)
        print 'connect {}'.format(self.connectedSid)
        self.activity()
        self.bestWhenOffset = self.offsetOf(when)  # Store initial time offset
        return self.connectedSid

    @public
    @synchronized(lock)
    def close(self, sid):
        '''Drop connected session'''
        self.checkSid(sid)
        self.driver.stop()
        self.camera.stop()
        self.connectedSid = None
        gpio.output(gpio.pinRemoteConnected, gpio.LOW)

    @public
    @synchronized(lock)
    def driveMotorSteer(self, sid, when, motor, steer):
        '''Drive the driver'''
        self.checkSid(sid)
        self.checkLag(when)
        self.activity()
        self.driver.driveMotorSteer(motor, steer)

    @public
    @synchronized(lock)
    def driveSpeedAngle(self, sid, when, speed, angle):
        '''Drive the driver'''
        self.checkSid(sid)
        self.checkLag(when)
        self.activity()
        self.driver.driveSpeedAngle(speed, angle)

    @public
    @synchronized(lock)
    def driveTracks(self, sid, when, left, right):
        '''Drive the driver'''
        self.checkSid(sid)
        self.checkLag(when)
        self.activity()
        self.driver.driveTracks(left, right)

    @public
    @synchronized(lock)
    def headlights(self, sid, when, on):
        '''Turn the headlights on or off'''
        self.checkSid(sid)
        self.checkLag(when)
        self.activity()
        self.driver.headlights(on)

    @public
    @jsonpickleEncode
    @synchronized(lock)
    def ping(self, sid, when):
        '''Ping the driver and the camera'''
        self.checkSid(sid)
        self.checkLag(when)
        self.activity()
        return SysInfo()

    @public
    @jsonpickleEncode
    @synchronized(lock)
    def getCameraConfigurations(self, sid):
        '''Return camera configurations'''
        # TODO There is no point in having this sid-checked, other than the bad design on client side. :(
        self.checkSid(sid)
        self.activity()
        return self.camera.getConfigurations()

    @public
    @synchronized(lock)
    def startCamera(self, sid, when, id, clientAddress):
        self.checkSid(sid)
        self.checkLag(when)
        self.activity()
        return self.camera.start(id, clientAddress)

    @public
    @synchronized(lock)
    def stopCamera(self, sid):
        self.checkSid(sid)
        self.activity()
        self.camera.stop()
