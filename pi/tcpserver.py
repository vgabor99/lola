#!/usr/bin/env python
# coding: utf-8
import SocketServer

from tinyrpc import RPCError
from tinyrpc.dispatch import RPCDispatcher
from tinyrpc.protocols.jsonrpc import JSONRPCProtocol, JSONRPCInternalError

from app import rpc_methods

# Connect RPC methods to the dispatcher.
dispatcher = RPCDispatcher()
dispatcher.method_map.update(rpc_methods)

protocol = JSONRPCProtocol()


class TcpRequestHandler(SocketServer.StreamRequestHandler):
    '''RPC over TCP request handler'''

    def setup(self):
        self.disable_nagle_algorithm = True  # Disable socket-level buffering
        self.wbufsize = 1  # Buffer until newline.
        SocketServer.StreamRequestHandler.setup(self)

    # Handle a TCP request.
    # We keep the socket alive and process multiple RPC requests.
    # So one request is a complete RPC session, with one RPC request per line.
    # An empty request line is the disconnect signal.
    def handle(self):
        try:
            print "{} connected".format(self.client_address[0])
            while True:
                requestStr = self.rfile.readline().strip()
                print "{} >> {}".format(self.client_address[0], requestStr)
                if (len(requestStr) == 0):
                    print "{} disconnected".format(self.client_address[0])
                    break  # Empty string is the disconnect message
                responseStr = self.rpc(requestStr)
                print "{} << {}".format(self.client_address[0], responseStr)
                self.wfile.write(responseStr)
                self.wfile.write('\n')
                self.wfile.flush()

        finally:
            print "{} done".format(self.client_address[0])

    # Handle an RPC request.
    def rpc(self, requestStr):

        try:
            request = protocol.parse_request(requestStr)
        except RPCError as e:
            response = e.error_respond()
        else:
            response = dispatcher.dispatch(request)

        # Serializing the response can also fail.
        # That is a server error but make a response and keep going anyway.
        # FIXME this is definitely wrong!
        # FIXME How can I make it coming from the protocol and NOT hard-code JSON-RPC?
        # FIXME how can I set the id
        try:
            responseStr = response.serialize()
        except Exception as e:
            responseStr = JSONRPCInternalError(e).error_respond().serialize()
        # End of FIXME

        return responseStr


class PiRemoteTcpServer(SocketServer.ThreadingTCPServer):
    '''TCP server'''
    daemon_threads = True
    allow_reuse_address = True

    def __init__(self, server_address):
        SocketServer.TCPServer.__init__(self, server_address, TcpRequestHandler)


def run():
    HOST, PORT = "0.0.0.0", 9999
    server = PiRemoteTcpServer((HOST, PORT))
    server.serve_forever()


if __name__ == "__main__":
    run()
