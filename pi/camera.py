#!/usr/bin/env python
# coding: utf-8
import hashlib
import json
import socket
import subprocess
import time


class CameraConfig:
    '''Camera configuration.'''

    def __init__(self, description, width, height, connectionOriented, serverCmd, clientConfig):
        # Client UI information. Human readable description, and informational width/height so the view can be
        # sized early (without actually receiving pictures).
        self.description = description
        self.width = width
        self.height = height
        # Connection oriented or connectionless?
        # For connection oriented mode, the camera is started first and the client connects.
        # For connectionless mode, the player is first and must provide the client address.
        self.connectionOriented = connectionOriented
        # This is the server command that launches the camera.
        self.serverCmd = serverCmd
        # Client pipeline config.
        self.clientConfig = clientConfig
        # We just want an id that identifies the contained data. Nothing cryptographic here, so MD5 will do.
        # The id must remain stable because the client stores it as user preference.
        self.id = hashlib.md5(json.dumps(vars(self))).hexdigest()

    def __str__(self):
        return "{} ({})".format(self.id, self.description)


class Camera:
    '''Control the camera.'''

    # Camera configuration list.
    configList = [CameraConfig(*args) for args in
                  [
                      ('640 x 480 @ 25fps, 2Mbps, UDP',
                       640, 480, False,
                       'raspivid -t 0 -w 640 -h 480 -fps 25 -hf -vf -b 2000000 -o - | gst-launch-1.0 -v fdsrc ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host={clientHost} port={clientPort}',
                       'udpsrc port={clientPort} caps="application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! decodebin ! autovideosink sync=false'),

                      ('640 x 480 @ 25fps, 1Mbps, UDP',
                       640, 480, False,
                       'raspivid -t 0 -w 640 -h 480 -fps 25 -hf -vf -b 1000000 -o - | gst-launch-1.0 -v fdsrc ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host={clientHost} port={clientPort}',
                       'udpsrc port={clientPort} caps="application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! decodebin ! autovideosink sync=false'),

                      ('640 x 480 @ 25fps, 500kbps, UDP',
                       640, 480, False,
                       'raspivid -t 0 -w 640 -h 480 -fps 25 -hf -vf -b 500000 -o - | gst-launch-1.0 -v fdsrc ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host={clientHost} port={clientPort}',
                       'udpsrc port={clientPort} caps="application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! decodebin ! autovideosink sync=false'),

                      ('640 x 480 @ 25fps, 2Mbps, TCP',
                       640, 480, True,
                       'raspivid -t 0 -w 640 -h 480 -fps 25 -hf -vf -b 2000000 -o - | gst-launch-1.0 -v fdsrc ! h264parse ! rtph264pay config-interval=1 pt=96 ! gdppay ! tcpserversink host={serverHost} port={serverPort}',
                       'tcpclientsrc host={serverHost} port={serverPort} ! gdpdepay ! rtph264depay ! decodebin ! autovideosink sync=false')

                  ]]
    # Lookup map for the configurations
    configMap = {config.id: config for config in configList}

    def __init__(self):
        self.process = None

    def stop(self):
        '''Stop. Called by the Dead man's switch.'''
        print 'camera stop'
        try:
            subprocess.call("killall raspivid", shell=True)
        except:
            pass

    def getConfigurations(self):
        return self.configList

    def start(self, id, clientAddress):
        '''
        Start the camera.

        :param id: Camera config id.
        :param clientAddress: Client sink address (connectionless mode), or None (connection oriented mode)
        :return: None (connectionless mode), or server source address (connection oriented mode)
        '''
        self.stop()
        cameraConfig = Camera.configMap[id]
        if (cameraConfig.connectionOriented):
            clientHost, clientPort = None, None
            serverHost, serverPort = getMyAddress(), 9998  # TODO we use a fixed port for now
        else:
            if (not clientAddress):
                raise Exception, 'Connectionless mode needs clientAddress'
            clientHost, clientPort = clientAddress.split(':')
            #clientHost = '192.168.0.255'  # TODO TEMP broadcast for debugging
            serverHost, serverPort = None, None
        cmd = cameraConfig.serverCmd.format(clientHost=clientHost, clientPort=clientPort, serverHost=serverHost,
                                            serverPort=serverPort)
        print 'camera start'
        print cmd
        self.process = subprocess.Popen(cmd, shell=True)
        if (cameraConfig.connectionOriented):
            time.sleep(0.1)  # It seems that must wait a bit or the client can't connect properly. :(
            return "{}:{}".format(serverHost, serverPort)

    def isRunning(self):
        return self.process is not None and self.process.poll() is None


def getMyAddress():
    '''Hack to get local IP.'''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 0))  # connecting to a UDP address doesn't send packets
    result = s.getsockname()[0]
    print result
    return result
