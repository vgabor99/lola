#!/usr/bin/env python
# coding: utf-8

import argparse
import logging.handlers
from threading import Thread, Event

# Defaults
from tcpserver import PiRemoteTcpServer
from httpserver import PiRemoteHttpServer

LOG_FILENAME = "/tmp/piremote.log"
LOG_LEVEL = logging.INFO  # Could be e.g. "DEBUG" or "WARNING"

# Define and parse command line arguments
parser = argparse.ArgumentParser(description="Pi Remote service")
parser.add_argument("-l", "--log", help="file to write log to (default '" + LOG_FILENAME + "')")

# If the log file is specified on the command line then override the default
args = parser.parse_args()
if args.log:
    LOG_FILENAME = args.log

logger = logging.getLogger(__name__)
logger.setLevel(LOG_LEVEL)
handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="h", backupCount=2)
formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


class MyLogger(object):
    def __init__(self, logger, level):
        """Needs a logger and a logger level."""
        self.logger = logger
        self.level = level

    def write(self, message):
        # Only log if there is a message (not just a new line)
        if message.rstrip() != "":
            self.logger.log(self.level, message.rstrip())


# sys.stdout = MyLogger(logger, logging.INFO)
# sys.stderr = MyLogger(logger, logging.ERROR)


class TcpServerThread(Thread):
    def __init__(self, done):
        Thread.__init__(self)
        HOST, PORT = "0.0.0.0", 9999
        self.server = PiRemoteTcpServer((HOST, PORT))
        self.done = done

    def run(self):
        try:
            print 'TCP server thread start'
            self.server.serve_forever()
        finally:
            done.set()
            print 'TCP server thread done'


class HttpServerThread(Thread):
    def __init__(self, done):
        Thread.__init__(self)
        HOST, PORT = "0.0.0.0", 8888
        self.server = PiRemoteHttpServer((HOST, PORT))
        self.done = done

    def run(self):
        try:
            print 'HTTP server thread start'
            self.server.serve_forever()
        finally:
            done.set()
            print 'HTTP server thread done'


if __name__ == '__main__':
    tcpServerThread = None
    httpServerThread = None
    try:
        done = Event()
        tcpServerThread = TcpServerThread(done)
        tcpServerThread.setName('TcpServer')
        httpServerThread = HttpServerThread(done)
        httpServerThread.setName('HttpServer')
        tcpServerThread.start()
        httpServerThread.start()
        # We don't get signals properly when the background threads are blocking. To get the signals (e.g. Ctrl+C),
        # we must poll. So we use the timeout arg in Event.wait()
        while not done.wait(0.5):
            pass
    finally:
        if tcpServerThread is not None and tcpServerThread.isAlive():
            tcpServerThread.server.shutdown()
        if httpServerThread is not None and httpServerThread.isAlive():
            httpServerThread.server.shutdown()
