#!/usr/bin/env python
# coding: utf-8

import pyjsonrpc

from app import rpc_methods


class HttpRequestHandler(pyjsonrpc.HttpRequestHandler):
    def setup(self):
        self.disable_nagle_algorithm = True  # Disable socket-level buffering
        pyjsonrpc.HttpRequestHandler.setup(self)
        self.methods.update(rpc_methods)

    def end_headers(self):
        """Make sure that all of our responses have CORS headers"""
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "Accept, Content-Type")
        super(HttpRequestHandler, self).end_headers()

    def do_OPTIONS(self):
        """Enable CORS, for now we accept anything from anywhere"""
        self.send_response(200, "ok")
        self.set_content_type("text/html")
        self.set_content_length(0)
        self.end_headers()


class PiRemoteHttpServer(pyjsonrpc.ThreadingHttpServer):
    '''HTTP server'''
    daemon_threads = True
    allow_reuse_address = True

    def __init__(self, server_address):
        pyjsonrpc.ThreadingHttpServer.__init__(self, server_address, HttpRequestHandler)


def run():
    HOST, PORT = "0.0.0.0", 8888
    server = PiRemoteHttpServer((HOST, PORT))
    server.serve_forever()


if __name__ == "__main__":
    run()
