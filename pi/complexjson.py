#!/usr/bin/env python
# coding: utf-8
from functools import wraps

import jsonpickle


def jsonpickleEncode(func):
    '''
    Decorator that wraps with jsonpickle.

    tinyrpc uses json.dumps() which does not work with complex objects. jsonpickle to the rescue,
    '''

    @wraps(func)
    def wrapper(*args, **kwargs):
        return jsonpickle.encode(func(*args, **kwargs), unpicklable=False)

    return wrapper
